﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class AppLlegadaOrdenesDiarias
    {

        [Key]
        public int Id { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }
        public DateTime Dia { get; set; }
        public string DiaChar { get; set; }
        public int CantOrdenes { get; set; }
        public decimal BsOrdenes { get; set; }


     

    }
}
