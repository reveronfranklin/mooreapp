namespace MooreApp.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class WSMY503
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }

        [StringLength(1)]
        public string Flag_Ganada { get; set; }

        [StringLength(1)]
        public string Flag_Modificar { get; set; }

        [StringLength(1)]
        public string Flag_Activa { get; set; }

        [StringLength(1)]
        public string Flag_Postergada { get; set; }

        [StringLength(1)]
        public string Flag_Perdida { get; set; }

        [StringLength(1)]
        public string Flag_Caducada { get; set; }

        [StringLength(1)]
        public string Flag_EnEspera { get; set; }

        [StringLength(1)]
        public string Flag_CaducaInactiva { get; set; }

        [StringLength(3)]
        public string Abreviado { get; set; }

        [StringLength(1)]
        public string PrimeraEstacion { get; set; }

        public decimal? CondicionRazonId { get; set; }
    }
}
