﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class ConsultaPrecio
    {
        [Key]
        public int Id { get; set; }

        public string Producto { get; set; }

        public string Estado { get; set; }

        [Display(Name = "Capital Municipio")]
        public string Municipio { get; set; }

        [Display(Name = "Precio Minimo")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public decimal PrecioMinimo { get; set; }

        [Display(Name = "Precio Maximo")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public decimal PrecioMaximo { get; set; }

    }
}
