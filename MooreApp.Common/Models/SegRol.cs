﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{

    public partial class SegRol
    {
        public SegRol()
        {
           
            this.SegUsuarioRol = new HashSet<SegUsuarioRol>();
        }
        [Key]
        public int IdRol { get; set; }
        public string NombreRol { get; set; }
        public int IdPrograma { get; set; }
        public string DescripcionRol { get; set; }
        public string AbreviadoRol { get; set; }

    
        public virtual ICollection<SegUsuarioRol> SegUsuarioRol { get; set; }
    }
}
