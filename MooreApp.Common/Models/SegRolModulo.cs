﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{

    public partial class SegRolModulo
    {
        [Key]
        public int IdRolModulo { get; set; }
        public int IdRol { get; set; }
        public int IdModulo { get; set; }
    }
}
