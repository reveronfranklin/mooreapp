namespace MooreApp.DatabaseFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WSMY501
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WSMY501()
        {
            WSMY502 = new HashSet<WSMY502>();
        }

        [Key]
        [StringLength(13)]
        public string Cotizacion { get; set; }

        [StringLength(4)]
        public string Cod_Vendedor { get; set; }

        [StringLength(10)]
        public string Cod_Cliente { get; set; }

        [StringLength(1)]
        public string Prospecto { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha { get; set; }

        [StringLength(250)]
        public string Observaciones { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha_Caducidad { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha_Postergada { get; set; }

        public int? Estatus { get; set; }

        [StringLength(80)]
        public string Razon_Social { get; set; }

        public int? Tipo { get; set; }

        public int? Sector { get; set; }

        public int? Ramo { get; set; }

        [StringLength(60)]
        public string Email_Cliente { get; set; }

        [StringLength(12)]
        public string Rif { get; set; }

        public int? Condicion { get; set; }

        public int? Id_Contacto { get; set; }

        [StringLength(500)]
        public string Observacion_Postergar { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha_EnEspera { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha_Cerrada { get; set; }

        [StringLength(50)]
        public string UsuarioActualiza { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FechaActualiza { get; set; }

        [StringLength(250)]
        public string NombreContacto { get; set; }

        [StringLength(250)]
        public string TelefonoContacto { get; set; }

        [StringLength(250)]
        public string EmailContacto { get; set; }

        public decimal? Id_direccion { get; set; }

        [StringLength(240)]
        public string DireccionFacturar { get; set; }

        [StringLength(240)]
        public string DireccionEntregar { get; set; }

        [StringLength(2)]
        public string FlagWebSite { get; set; }

        [StringLength(12)]
        public string RifEntregar { get; set; }

        [StringLength(16)]
        public string OrdenCompra { get; set; }

        public short? IdMedio { get; set; }

        [StringLength(1)]
        public string FlagActualizado { get; set; }

        public decimal? Id_direccionEntregar { get; set; }

        [StringLength(2)]
        public string EstadoFacturar { get; set; }

        [StringLength(2)]
        public string MunicipioFacturar { get; set; }

        [StringLength(2)]
        public string EstadoEntregar { get; set; }

        [StringLength(2)]
        public string MunicipioEntregar { get; set; }

        public bool? FlagValidado { get; set; }

        public DateTime? FechaReactivacion { get; set; }

        [StringLength(10)]
        public string IdClienteProspecto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? idsolicitudCredito { get; set; }

        [StringLength(40)]
        public string UltimoEvaluadorCredito { get; set; }

        public bool? EvaluadaAdministradora { get; set; }

        public bool? CambioPrecio { get; set; }

        public bool? Noreplicar { get; set; }

        [Required]
        [StringLength(4)]
        public string IdTipoTransaccion { get; set; }

        public bool? NegocioEnUsd { get; set; }

        public bool? FijarPrecioBs { get; set; }

        public bool? FijarPrecioBsAprobado { get; set; }

        public DateTime? FechaPago { get; set; }

        public string ObservacionPago { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WSMY502> WSMY502 { get; set; }
    }
}
