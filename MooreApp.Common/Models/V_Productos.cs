﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class V_Productos
    {
        [Key]
        public string PRODUCTO { get; set; }

        public string DESCRIPCION { get; set; }

        public int CANTIDAD_X_CAJA { get; set; }

        public int UNIDAD_COSTEO { get; set; }


    }
}
