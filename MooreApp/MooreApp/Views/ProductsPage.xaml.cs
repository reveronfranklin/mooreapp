﻿using MooreApp.Common.Models;
using MooreApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MooreApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProductsPage : ContentPage
	{
		public ProductsPage ()
		{
			InitializeComponent ();
		}

        public async void ProductClicked(object sender, ItemTappedEventArgs e)
        {
            V_Productos productsItem = (V_Productos)e.Item;
           

           MainViewModel.GetInstance().PreciosStock = new PreciosStockViewModel(productsItem);

            await App.Navigator.PushAsync(new PreciosStockPage());
        }


    }
}