﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{

    public partial class SegPrograma
    {
        [Key]
        public int IdPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public string Descripcion { get; set; }
        public string TipoPrograma { get; set; }
        public Nullable<bool> ParaMenu { get; set; }
        public string link { get; set; }
        public string FlagRequerimiento { get; set; }
        public byte IdAreaNegocio { get; set; }
        public short ServidorReporte { get; set; }
        public short ServidorPublico { get; set; }
    }
}
