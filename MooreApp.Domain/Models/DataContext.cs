﻿

namespace MooreApp.Domain.Models
{


    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class DataContext: DbContext
    {

        public DataContext():base("DefaultConnection")
        {


        }
        
        public System.Data.Entity.DbSet<MooreApp.Common.Models.PreciosStockEstadoMunicipios> PreciosStockEstadoMunicipios { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.V_Productos> V_Productos { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.WITY021> WITY021 { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.WITY020> WITY020 { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.AppUsuarioPages> AppUsuarioPages { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.VEstados> VEstados { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.VMunicipios> VMunicipios { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.AppLlegadaOrdenesDiarias> AppLlegadaOrdenesDiarias { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.AppBacklogInicial> AppBacklogInicial { get; set; }

        

        public System.Data.Entity.DbSet<MooreApp.Common.Models.AppLlegadaOrdenesDiariasOficina> AppLlegadaOrdenesDiariasOficina { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.AppOrdenesGrabadasProducto> AppOrdenesGrabadasProducto { get; set; }


        

        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegTicket> SegTicket { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegModulo> SegModulo { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegPrograma> SegPrograma { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegUsuarioRol> SegUsuarioRol { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegRol> SegRol { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegRolModulo> SegRolModulo { get; set; }
        public System.Data.Entity.DbSet<MooreApp.Common.Models.SegUsuario> SegUsuario { get; set; }


        public System.Data.Entity.DbSet<MooreApp.Common.Models.WSMY501> WSMY501 { get; set; }

        public System.Data.Entity.DbSet<MooreApp.Common.Models.WSMY502> WSMY502 { get; set; }

        public System.Data.Entity.DbSet<MooreApp.Common.Models.WSMY503> WSMY503 { get; set; }

        public System.Data.Entity.DbSet<MooreApp.Common.Models.WSMY515> WSMY515 { get; set; }


        public System.Data.Entity.DbSet<MooreApp.Common.Models.PCVendedor> PCVendedor { get; set; }

        protected override void OnModelCreating(DbModelBuilder dbModelBuilder)
        {
            dbModelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<MooreApp.Common.Models.ConsultaPrecio> ConsultaPrecios { get; set; }
    }


}

