﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class Estado
    {
        public string  CodEstado { get; set; }
        public string  Nombre { get; set; }

        public override string ToString()
        {
            return this.CodEstado  + "-"+ this.Nombre ;
        }
    }
}
