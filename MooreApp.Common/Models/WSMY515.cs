namespace MooreApp.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class WSMY515
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(13)]
        public string Cotizacion { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Renglon { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Propuesta { get; set; }

        public decimal? Cantidad { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PrecioUnitario { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalPropuesta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorMC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BsMC { get; set; }

        public int? Estatus { get; set; }

        [StringLength(100)]
        public string ImpresionTiro { get; set; }

        [StringLength(100)]
        public string ImpresionRetiro { get; set; }

        public int? Medidas { get; set; }

        [StringLength(50)]
        public string Papel { get; set; }

        public int? Partes { get; set; }

        [StringLength(100)]
        public string Respaldo { get; set; }

        [StringLength(50)]
        public string Medida_Texto { get; set; }

        public DateTime? FechaRegistro { get; set; }

        [StringLength(200)]
        public string Observaciones { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LoteAct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cant_x_Caja { get; set; }

        public float? Cant_Mill { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cajas { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cant_Formas { get; set; }

        [Required]
        [StringLength(9)]
        public string IdSolicitud { get; set; }

        [StringLength(1)]
        public string RecalcularMargen { get; set; }

        [StringLength(20)]
        public string CotizacionRenglonPropuesta { get; set; }

        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal Id { get; set; }

        public short? UnidadCosteo { get; set; }

        [StringLength(5)]
        public string IdPresentacion { get; set; }

        [StringLength(4)]
        public string EstatusPlanta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcRespSocial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcComRegulada { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcTolerancia { get; set; }

        public string MensajeError { get; set; }

        public bool? FlagAprobado { get; set; }

        public short? CambComposicion { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorMcFinan { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcMcMinimo { get; set; }

        public bool? FlagVolumen { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorMcBruto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcComision { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FactorSdf { get; set; }

        public int? IdTablaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BsListaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalBsListaCPJ { get; set; }

        [StringLength(1)]
        public string FlagTipoRechazo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcLista { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BsListaCPJMinimo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalBsListaCPJMinimo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PrecioEtiqueta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PorcIva { get; set; }

        public bool? CambioPrecio { get; set; }

        public bool? EvaluarCredito { get; set; }

        public long Orden { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RPrecioUnitario { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RTotalPropuesta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RBsMC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RBsListaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RTotalBsListaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RBsListaCPJMinimo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RTotalBsListaCPJMinimo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RPrecioEtiqueta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PrecioUnitarioUsd { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalPropuestaUsd { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UsdListaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalUsdListaCPJ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? UsdListaCPJMinimo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalUsdListaCPJMinimo { get; set; }
    }
}
