﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class AñoMesRequest
    {
        public int Año { get; set; }

        public int Mes { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }

    }
}
