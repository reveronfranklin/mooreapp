﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MooreApp.Helpers;
using MooreApp.Views;
using Xamarin.Forms;

namespace MooreApp.ViewModels
{
    public class MenuItemViewModel
    {

        #region Properties
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }

        #endregion

        #region Command

        public ICommand GotoCommand
        {
            get
            {
                return new RelayCommand(Goto);
            }

        }

        void Goto()
        {
            if (this.PageName=="LoginPage")
            {
                Settings.IsRemembered = false;
                Settings.Password = string.Empty;
                Settings.Usuario = string.Empty;
                MainViewModel.GetInstance().Login = new LoginViewModel();
                Application.Current.MainPage = new NavigationPage(new LoginPage());
            }
            if (this.PageName == "ReportCategoriasPage")
            {
               
                MainViewModel.GetInstance().ReportCategorias = new ReportCategoriasViewModel();

                App.Master.IsPresented = false;
                App.Navigator.PushAsync(new ReportCategoriasPage());
               // Application.Current.MainPage = new NavigationPage(new ReportCategoriasPage());
            }
            if (this.PageName == "ReportCategoriasHistoricoPage")
            {

                MainViewModel.GetInstance().ReportCategoriasHistorico = new ReportCategoriasHistoricoViewModel();

                App.Master.IsPresented = false;
                App.Navigator.PushAsync(new ReportCategoriasHistoricoPage());
                // Application.Current.MainPage = new NavigationPage(new ReportCategoriasPage());
            }
            if (this.PageName == "OrdenesRecibidasPage")
            {

               
                App.Master.IsPresented = false;
                App.Navigator.PushAsync(new OrdenesRecibidasPage());
                // Application.Current.MainPage = new NavigationPage(new ReportCategoriasPage());
            }
        } 

        #endregion

    }
}
