﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class ReportCategoriaHistorico
    {

        public int IdCategoria { get; set; }

        public string Categoria { get; set; }

        public int IdSubCategoria { get; set; }

        public string SubCategoria { get; set; }

        public decimal ?EmbarquesBolivares { get; set; }
        

    }
}
