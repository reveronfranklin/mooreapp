﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class PreciosStockEstadoMunicipios
    {
        [Key]
        public int Id { get; set; }

       
        [Required]
        public string Producto { get; set; }
        [MaxLength(2)]
        [Required]
        public string Estado { get; set; }
        [MaxLength(2)]
        [Required]
        public string Municipio { get; set; }

        [MaxLength(150)]
        [Required]
        public string Descripcion { get; set; }

        [MaxLength(150)]
        [Required]
        [Display(Name = "Estado")]
        public string NombreEstado { get; set; }
        [MaxLength(50)]
        [Required]
        [Display(Name = "Municipio")]
        public string NombreMunicipio { get; set; }
        [MaxLength(50)]
        [Required]
        [Display(Name = "Capital Municipio")]
        public string CapitalMunicipio { get; set; }

        [Display(Name = "Precio Minimo")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public decimal PrecioMinimo { get; set; }

        [Display(Name = "Precio Maximo")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public decimal PrecioMaximo { get; set; }

        public int IdCalculo { get; set; }

        [Display(Name = "Fecha Calculo")]
        public DateTime? FechaCalculo { get; set; }

       
        public override string ToString()
        {
            return this.Descripcion;
        }
    }
}
