﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class AppLlegadaOrdenesDiariasOficina
    {

        [Key]
        public int Id { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }
        public string Oficina { get; set; }
        public int CantOrdenes { get; set; }
        public decimal BsOrdenes { get; set; }


    }
}
