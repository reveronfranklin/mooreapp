﻿using MooreApp.Common.Models;
using MooreApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MooreApp.Backend.Controllers
{
    public class VisualizeDataController : Controller
    {
        // GET: VisualizeData
        public async Task<ActionResult>  ColumnChart()
        {
            

            using (var db = new DataContext())
            {
                string tituloGraficaDiaria = "";



                bool Actualizado = false;
                AppOrdenesGrabadasProducto ordenes = new AppOrdenesGrabadasProducto();
                ordenes = db.AppOrdenesGrabadasProducto.Where(o => o.DiaChar == "").FirstOrDefault();
                if (ordenes == null)
                {
                    Actualizado = true;
                }

                AppOrdenesGrabadasProducto FirstDay = new AppOrdenesGrabadasProducto();
                FirstDay = db.AppOrdenesGrabadasProducto.FirstOrDefault();
                if (FirstDay!=null)
                {
                    ViewBag.TituloGraficaDiaria = "Llegada de Ordenes Diarias " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();
                    ViewBag.TituloGraficaDiariaOficina = "Ordenes Grabadas por Oficina " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();
                    ViewBag.TituloTablaDiaria = "Bs.Grabados y Proyección de Embarques " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();
                    
                }
                AppOrdenesGrabadasProducto RegTotals = new AppOrdenesGrabadasProducto();
                RegTotals = db.AppOrdenesGrabadasProducto.Where(a=>a.DiaChar=="TOTAL").FirstOrDefault();
                if (RegTotals != null)
                {
                    ViewBag.CantTotalFormas = RegTotals.CantFormas.ToString();
                    ViewBag.BsTotalFormas = RegTotals.BsFormasChar;

                    ViewBag.CantTotalEtiquetas = RegTotals.CantEtiquetas.ToString();
                    ViewBag.BsTotalEtiquetas = RegTotals.BsEtiquetasChar;

                    ViewBag.CantTotalICPP = RegTotals.CantICPP.ToString();
                    ViewBag.BsTotalICPP = RegTotals.BsICPPChar;

                    ViewBag.CantTotalCustomPrint = RegTotals.CantCustomPrint.ToString();
                    ViewBag.BsTotalCustomPrint = RegTotals.BsCustomPrintChar;

                    ViewBag.CantTotalOfficeProduct = RegTotals.CantOfficeProduct.ToString();
                    ViewBag.BsTotalOfficeProduct = RegTotals.BsOfficeProductChar;

                    ViewBag.CantTotalEmpresa = RegTotals.CantTotal.ToString();
                    ViewBag.BsTotalEmpresa = RegTotals.BsTotalChar;

                    ViewBag.USDTotalEmpresa = RegTotals.VentaDolarChar;

                    AppBacklogInicial appBacklogInicial = new AppBacklogInicial();

                    appBacklogInicial = db.AppBacklogInicial.Where(a => a.Año == RegTotals.Año && a.Mes==RegTotals.Mes).FirstOrDefault();
                    if (appBacklogInicial!=null)
                    {
                        ViewBag.CantBacklogInicial = appBacklogInicial.CantBacklogInicial.ToString();
                        ViewBag.BsBacklogInicial = DoFormatSinDecimales(appBacklogInicial.BsBacklogInicial);
                        ViewBag.USDBacklogInicial = DoFormatSinDecimales(appBacklogInicial.USDBacklogInicial);
                        ViewBag.DifCantBacklogInicial = RegTotals.CantTotal + appBacklogInicial.CantBacklogInicial;
                        ViewBag.DifBsBacklogInicial = DoFormatSinDecimales(RegTotals.BsTotal + appBacklogInicial.BsBacklogInicial );
                        ViewBag.DifUSDBacklogInicial = DoFormatSinDecimales(RegTotals.VentaDolar + appBacklogInicial.USDBacklogInicial);

                    }


                }
                if (!Actualizado)
                {
                    List<AppOrdenesGrabadasProducto> diarias = new List<AppOrdenesGrabadasProducto>();
                    diarias = db.AppOrdenesGrabadasProducto.OrderBy(d => d.Dia).ToList();
                    if (diarias != null)
                    {

                        foreach (var item in diarias)
                        {
                            if (item.DiaChar != "SUB-TOTAL" && item.DiaChar != "AJUSTES MES ANT." && item.DiaChar != "TOTAL")
                            {
                                item.DiaChar = item.Dia.Day.ToString() + "-" + GetMonthChar(item.Dia.Month);
                            }
                            item.BsCustomPrintChar = DoFormatSinDecimales(item.BsCustomPrint);
                            item.BsEtiquetasChar= DoFormatSinDecimales(item.BsEtiquetas);
                            item.BsFormasChar= DoFormatSinDecimales(item.BsFormas);
                            item.BsICPPChar = DoFormatSinDecimales(item.BsICPP);
                            item.BsOfficeProductChar = DoFormatSinDecimales(item.BsOfficeProduct);
                            item.BsTotalChar = DoFormatSinDecimales(item.BsTotal);
                            item.VentaDolarChar = DoFormatSinDecimales(item.VentaDolar);
                            db.Entry(item).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                    }
                }

                ViewBag.Fecha = DateTime.Now.ToString();

                var ordenesDiarias = await db.AppOrdenesGrabadasProducto.Where(a=> a.DiaChar != "TOTAL").ToListAsync();
                return View(ordenesDiarias);

               

            }





           


            //return View();
        }
        public async Task<ActionResult> TablaOrdenes()
        {


            using (var db = new DataContext())
            {
                



                bool Actualizado = false;
                AppOrdenesGrabadasProducto ordenes = new AppOrdenesGrabadasProducto();
                ordenes = db.AppOrdenesGrabadasProducto.Where(o => o.DiaChar == "").FirstOrDefault();
                if (ordenes == null)
                {
                    Actualizado = true;
                }
                if (!Actualizado)
                {
                    List<AppOrdenesGrabadasProducto> diarias = new List<AppOrdenesGrabadasProducto>();
                    diarias = db.AppOrdenesGrabadasProducto.OrderBy(d => d.Dia).ToList();
                    if (diarias != null)
                    {

                        foreach (var item in diarias)
                        {

                            if (item.DiaChar != "SUB-TOTAL" && item.DiaChar != "AJUSTES MES ANT." && item.DiaChar != "TOTAL")
                            {
                                item.DiaChar = item.Dia.Day.ToString() + "-" + GetMonthChar(item.Dia.Month);
                            }
                            item.BsCustomPrintChar = DoFormatSinDecimales(item.BsCustomPrint);
                            item.BsEtiquetasChar = DoFormatSinDecimales(item.BsEtiquetas);
                            item.BsFormasChar = DoFormatSinDecimales(item.BsFormas);
                            item.BsICPPChar = DoFormatSinDecimales(item.BsICPP);
                            item.BsOfficeProductChar = DoFormatSinDecimales(item.BsOfficeProduct);
                            item.BsTotalChar = DoFormatSinDecimales(item.BsTotal);
                            item.VentaDolarChar = DoFormatSinDecimales(item.VentaDolar);
                            db.Entry(item).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                    }
                }
                AppOrdenesGrabadasProducto FirstDay = new AppOrdenesGrabadasProducto();
                FirstDay = db.AppOrdenesGrabadasProducto.FirstOrDefault();
                if (FirstDay != null)
                {
                    ViewBag.TituloGraficaDiaria = "Llegada de Ordenes Diarias " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();
                    ViewBag.TituloGraficaDiariaOficina = "Ordenes Grabadas por Oficina " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();
                    ViewBag.TituloTablaDiaria = "USD Grabados y Proyección de Embarques " + GetMonthCompletChar(FirstDay.Mes) + " " + FirstDay.Año.ToString();

                }
                AppOrdenesGrabadasProducto RegTotals = new AppOrdenesGrabadasProducto();
                RegTotals = db.AppOrdenesGrabadasProducto.Where(a => a.DiaChar == "TOTAL").FirstOrDefault();
                if (RegTotals != null)
                {
                    ViewBag.CantTotalFormas = RegTotals.CantFormas.ToString();
                    ViewBag.BsTotalFormas = RegTotals.BsFormasChar;

                    ViewBag.CantTotalEtiquetas = RegTotals.CantEtiquetas.ToString();
                    ViewBag.BsTotalEtiquetas = RegTotals.BsEtiquetasChar;

                    ViewBag.CantTotalICPP = RegTotals.CantICPP.ToString();
                    ViewBag.BsTotalICPP = RegTotals.BsICPPChar;

                    ViewBag.CantTotalCustomPrint = RegTotals.CantCustomPrint.ToString();
                    ViewBag.BsTotalCustomPrint = RegTotals.BsCustomPrintChar;

                    ViewBag.CantTotalOfficeProduct = RegTotals.CantOfficeProduct.ToString();
                    ViewBag.BsTotalOfficeProduct = RegTotals.BsOfficeProductChar;

                    ViewBag.CantTotalEmpresa = RegTotals.CantTotal.ToString();
                    ViewBag.BsTotalEmpresa = RegTotals.BsTotalChar;
                    ViewBag.USDTotalEmpresa = RegTotals.VentaDolarChar;


                    AppBacklogInicial appBacklogInicial = new AppBacklogInicial();

                    appBacklogInicial = db.AppBacklogInicial.Where(a => a.Año == RegTotals.Año && a.Mes == RegTotals.Mes).FirstOrDefault();
                    if (appBacklogInicial != null)
                    {
                        ViewBag.CantBacklogInicial = appBacklogInicial.CantBacklogInicial.ToString();
                        ViewBag.BsBacklogInicial = DoFormatSinDecimales(appBacklogInicial.BsBacklogInicial);
                        ViewBag.USDBacklogInicial = DoFormatSinDecimales(appBacklogInicial.USDBacklogInicial);


                        ViewBag.DifCantBacklogInicial = RegTotals.CantTotal + appBacklogInicial.CantBacklogInicial;
                        ViewBag.DifBsBacklogInicial = DoFormatSinDecimales(RegTotals.BsTotal + appBacklogInicial.BsBacklogInicial);
                        ViewBag.DifUSDBacklogInicial = DoFormatSinDecimales(RegTotals.VentaDolar + appBacklogInicial.USDBacklogInicial);

                    }


                }
               

                var ordenesDiarias = await db.AppOrdenesGrabadasProducto.Where(a => a.DiaChar != "TOTAL").ToListAsync();
               
                return View(ordenesDiarias);



            }








            //return View();
        }
        public ActionResult VisualizeOrdenesResults()
        {
            return Json(Result(),JsonRequestBehavior.AllowGet);
        }

        public ActionResult VisualizeOrdenesOficinaResults()
        {
            return Json(ResultOficina(), JsonRequestBehavior.AllowGet);
        }
        public List<CantidadOrdenesDiaiasResult> Result()
        {

            List<CantidadOrdenesDiaiasResult> stdResult = new List<CantidadOrdenesDiaiasResult>();



            using (var db = new DataContext())
            {

                List<AppLlegadaOrdenesDiarias> diarias = new List<AppLlegadaOrdenesDiarias>();
                diarias = db.AppLlegadaOrdenesDiarias.OrderBy(d => d.Dia).ToList();
                if (diarias != null)
                {

                    foreach (var item in diarias)
                    {
                        if (item.CantOrdenes!=0)
                        {
                            item.DiaChar = item.Dia.Day.ToString() + "-" + GetMonthChar(item.Dia.Month);
                            stdResult.Add(new CantidadOrdenesDiaiasResult()
                            {
                                CantidadOrdenes = item.CantOrdenes,
                                Dia = item.DiaChar,
                                Venta = item.BsOrdenes


                            });
                        }

                       
                    }
                }
            }




            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 88,
            //    Dia = "01-Jun",
            //    Venta = 5000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 150,
            //    Dia = "02-Jun",
            //    Venta = 8000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 23,
            //    Dia = "03-Jun",
            //    Venta = 7000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 55,
            //    Dia = "04-Jun",
            //    Venta = 500000

            //});

            return stdResult;
        }
        public List<CantidadOrdenesDiariasOficinaResult> ResultOficina()
        {

            List<CantidadOrdenesDiariasOficinaResult> stdResult = new List<CantidadOrdenesDiariasOficinaResult>();



            using (var db = new DataContext())
            {

                List<AppLlegadaOrdenesDiariasOficina> diarias = new List<AppLlegadaOrdenesDiariasOficina>();
                diarias = db.AppLlegadaOrdenesDiariasOficina.OrderByDescending(d => d.CantOrdenes ).ToList();
                if (diarias != null)
                {

                    foreach (var item in diarias)
                    {
                        if (item.CantOrdenes != 0)
                        {
                           
                            stdResult.Add(new CantidadOrdenesDiariasOficinaResult()
                            {
                                CantidadOrdenes = item.CantOrdenes,
                                Oficina = item.Oficina,
                                Venta = item.BsOrdenes


                            });
                        }


                    }
                }
            }




            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 88,
            //    Dia = "01-Jun",
            //    Venta = 5000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 150,
            //    Dia = "02-Jun",
            //    Venta = 8000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 23,
            //    Dia = "03-Jun",
            //    Venta = 7000000

            //});
            //stdResult.Add(new CantidadOrdenesDiaiasResult()
            //{
            //    CantidadOrdenes = 55,
            //    Dia = "04-Jun",
            //    Venta = 500000

            //});

            return stdResult;
        }

        public string GetMonthChar(int month)
        {
            string result = "";

            switch (month)
            {
                case 1:
                    result = "Ene";
                    break;
                case 2:
                    result = "Feb";
                    break;
                case 3:
                    result = "Mar";
                    break;
                case 4:
                    result = "Abr";
                    break;
                case 5:
                    result = "May";
                    break;
                case 6:
                    result = "Jun";
                    break;
                case 7:
                    result = "Jul";
                    break;
                case 8:
                    result = "Ago";
                    break;
                case 9:
                    result = "Sep";
                    break;
                case 10:
                    result = "oct";
                    break;
                case 11:
                    result = "Nov";
                    break;
                case 12:
                    result = "Dic";
                    break;

                default:
                    
                    break;
            }


            return result;
            
        }
        public string GetMonthCompletChar(int month)
        {
            string result = "";

            switch (month)
            {
                case 1:
                    result = "Enero";
                    break;
                case 2:
                    result = "Febrero";
                    break;
                case 3:
                    result = "Marzo";
                    break;
                case 4:
                    result = "Abril";
                    break;
                case 5:
                    result = "Mayo";
                    break;
                case 6:
                    result = "Junio";
                    break;
                case 7:
                    result = "Julio";
                    break;
                case 8:
                    result = "Agosto";
                    break;
                case 9:
                    result = "Septiembre";
                    break;
                case 10:
                    result = "Octubre";
                    break;
                case 11:
                    result = "Noviembre";
                    break;
                case 12:
                    result = "Diciembre";
                    break;

                default:

                    break;
            }


            return result;

        }
        public static string DoFormat(double myNumber)
        {



            var s = String.Format(CultureInfo.InvariantCulture,
            "{0:0,0.00}", myNumber);
            return s;

        }

        public static string DoFormat(decimal myNumber)
        {



            var s = String.Format(CultureInfo.InvariantCulture,
            "{0:0,0.00}", myNumber);
            return s;

        }
        public static string DoFormatSinDecimales(decimal myNumber)
        {



            var s = String.Format(CultureInfo.InvariantCulture,
            "{0:0,0}", myNumber);
            return s;

        }
    }
}