﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using MooreApp.Common.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MooreApp.Services;
using MooreApp.Views;
using MooreApp.Helpers;
using MooreApp.ViewModels;
namespace MooreApp.ViewModels
{
    public class LoginViewModel:BaseViewModel
    {
        #region Atributes
        private bool isEnable;
        private bool isRunning;
        private ApiServices apiServices;
        #endregion
        #region Properties
        public string  Email { get; set; }
        public string Password { get; set; }
        public bool IsRemembered { get; set; }


        public bool IsEnable
        {
            get { return this.isEnable; }
            set { this.SetValue(ref this.isEnable, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { this.SetValue(ref this.isRunning, value); }
        }

        #endregion

        #region Constructor
        public LoginViewModel()
        {
            this.apiServices = new ApiServices();
            this.IsEnable = true;
            this.IsRemembered = true;
        }
        #endregion
        #region Command
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }

        }

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error","Debes Ingresar Usuario","Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debes Ingresar Password", "Aceptar");
                return;
            }
            this.IsEnable = false;
            this.IsRunning = true;

            string urlBase = "http://mooreapps.com.ve/MooreApp";


            var loginRequest = new LoginRequest
            {
                Usuario = this.Email,
                 Password= this.Password,

            };



            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnable = true;
                await Application.Current.MainPage.DisplayAlert("Sin Conexion a internet!", connection.Message, "Aceptar");
                return;
            }
            var response = await this.apiServices.GetListUser<WITY021>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", loginRequest);

            if (!response.IsSuccess)
            {
                this.IsEnable = true;
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert("Error", "Usuario o Password Invalidos", "Aceptar");
                return;
               
            }

            var list = (List<WITY021>)response.Result;
            if (list.Count<=0)
            {
                this.IsEnable = true;
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert("Error", "Usuario o Password Invalidos", "Aceptar");
                return;
            }

            Settings.Password = this.Password;
            Settings.Usuario = this.Email;
            Settings.IsRemembered = this.IsRemembered;
            var main = MainViewModel.GetInstance();
            main.LoadMenu(Settings.Usuario, Settings.Password);

            MainViewModel.GetInstance().Products = new ProductsViewModel();
            Application.Current.MainPage = new MasterPage();

       
            //MainViewModel.GetInstance().Products = new ProductsViewModel();
            //Application.Current.MainPage = new ProductsPage();

            this.IsEnable = true;
            this.IsRunning = false;

        }





        #endregion
    }
}
