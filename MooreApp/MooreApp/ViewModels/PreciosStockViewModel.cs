﻿

using GalaSoft.MvvmLight.Command;
using MooreApp.Common.Models;
using MooreApp.Helpers;
using MooreApp.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using static System.Net.Mime.MediaTypeNames;

namespace MooreApp.ViewModels
{
    public class PreciosStockViewModel : BaseViewModel
    {

        private bool isRefreshing;
        private bool isEnable;
        private string estado;
        private string municipio;
        private string _producto;
        private string _descripcionProducto;
        private decimal _precioUnidad;
        private int _idCalculo;
        private ApiServices apiServices;
        private ObservableCollection<PreciosStockEstadoMunicipios> preciosStock;

        public ObservableCollection<PreciosStockEstadoMunicipios> PreciosStock
        {
            get { return this.preciosStock; }
            set { this.SetValue(ref this.preciosStock, value); }
        }


        private ObservableCollection<V_Productos> productdetail;

        public ObservableCollection<V_Productos> Productdetail
        {
            get { return this.productdetail; }
            set { this.SetValue(ref this.productdetail, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }

        public bool IsEnable
        {
            get { return this.isEnable; }
            set { this.SetValue(ref this.isEnable, value); }
        }
        public string Estado
        {
            get { return this.estado; }
            set { this.SetValue(ref this.estado, value); }
        }
        public string Municipio
        {
            get { return this.municipio; }
            set { this.SetValue(ref this.municipio, value); }
        }
        public string _Producto
        {
            get { return this._producto; }
            set { this.SetValue(ref this._producto, value); }
        }
        public string _DescripcionProducto
        {
            get { return this._descripcionProducto; }
            set { this.SetValue(ref this._descripcionProducto, value); }
        }
        public decimal _PrecioUnidad
        {
            get { return this._precioUnidad; }
            set { this.SetValue(ref this._precioUnidad, value); }
        }
        public int IdCalculo
        {
            get { return this._idCalculo; }
            set { this.SetValue(ref this._idCalculo, value); }
        }


       


        private V_Productos product;

        public PreciosStockViewModel(V_Productos product)
        {
            this.IsEnable = true;
            this.apiServices = new ApiServices();
            Productdetail = new ObservableCollection<V_Productos>();
            this.product = product;
            this.Productdetail.Add(product);
            this._Producto = this.product.PRODUCTO;
            this._DescripcionProducto = this.product.DESCRIPCION;
        }
        public ICommand LoadPrecioProductsCommand
        {
            get
            {
                return new RelayCommand(LoadPrecioProducts);
            }

        }

        private async void LoadPrecioProducts()
        {
            this.IsRefreshing = true;
            this.IsEnable = false;
            string urlBase = "https://mooreapps.com.ve/MooreApp";


            var productsViewModel = new ProductRequest
            {
                Producto = this.product.PRODUCTO,
                Estado = this.Estado,
                Municipio = this.Municipio,
                Usuario = Settings.Usuario,
                Password = Settings.Password,

            };
            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                this.IsEnable = true;
                 return;
            }

            var response = await this.apiServices.GetList<PreciosStockEstadoMunicipios>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", productsViewModel);

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                this.IsEnable = true;
                //await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Aceptar");

                return;
            }

            var list = (List<PreciosStockEstadoMunicipios>)response.Result;
            this.PreciosStock = new ObservableCollection<PreciosStockEstadoMunicipios>(list);
            var precio= this.PreciosStock.FirstOrDefault();
            decimal precioNumero;

            precioNumero = (decimal)precio.PrecioMinimo;
            this._PrecioUnidad = precioNumero;
            this.IsRefreshing = false;
            this.IsEnable = true;
            this.IdCalculo = precio.IdCalculo;
        }


        public void SetEstadoMunicipio(string codEstado,string codMunicipio)
        {
            this.Estado = codEstado;
            this.Municipio = codMunicipio; 
            this._Producto = this.product.PRODUCTO;
        }

        public void CleanProperty()
        {
            this._PrecioUnidad = 0;
        }

    }
}
