﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using MooreApp.Common.Models;
using MooreApp.Helpers;
using MooreApp.Services;

namespace MooreApp.ViewModels
{
    public class MainViewModel
    {
        private ApiServices apiServices;

        #region Properties

        public LoginViewModel Login { get; set; }

        public PreciosStockViewModel PreciosStock { get; set; }

        public ProductsViewModel Products { get; set; }

        public ReportCategoriasViewModel ReportCategorias { get; set; }
        public ReportCategoriasHistoricoViewModel ReportCategoriasHistorico { get; set; }


        public MenuItemViewModel MenuItem { get; set; }

        public ObservableCollection<MenuItemViewModel> Menu { get; set; }
        #endregion


        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        } 
        #endregion
        public MainViewModel()
        {
            this.apiServices = new ApiServices();
            instance = this;
            this.LoadMenu(Settings.Usuario,Settings.Password);
          
        }

        #region Methods
        public async void LoadMenu(string usuario,string password)
        {
            this.Menu = new ObservableCollection<MenuItemViewModel>();
            string urlBase = "https://mooreapps.com.ve/MooreApp";
            var loginRequest = new LoginRequest
            {
                Usuario = usuario,
                Password = password,

            };
            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {

                return;
            }
            var response = await this.apiServices.GetListUserPage<AppUsuarioPages>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", loginRequest);


            var list = (List<AppUsuarioPages>)response.Result;
            if (list != null)
            {
                foreach (var item in list)
                {
                    this.Menu.Add(new MenuItemViewModel
                    {
                        Icon = item.Icon,
                        PageName = item.Page,
                        Title = item.Title,
                    });
                }
            }

            
            this.Menu.Add(new MenuItemViewModel
            {
                Icon = "ic_Salir",
                PageName = "LoginPage",
                Title = "Cerrar sesion",
            });


        }
        #endregion

    }
}
