﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class ModeloUsuario
    {
        [Required]
        [StringLength(40)]
        [Display(Name = "Usuario: ")]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 1)]
        [Display(Name = "Contraseña: ")]
        public string Password { get; set; }

    }
}
