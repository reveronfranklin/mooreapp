﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
namespace MooreApp.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string usuario = "Usuario";
        private const string password = "Password";
        private const string isRemembered = "IsRemembered";
        private static readonly string stringDefault = string.Empty;
        private static readonly bool booleanDefault = false;

        #endregion


        public static string Usuario
        {
            get
            {
                return AppSettings.GetValueOrDefault(usuario, stringDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(usuario, value);
            }
        }


        public static string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault(password, stringDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(password, value);
            }
        }

        public static bool IsRemembered
        {
            get
            {
                return AppSettings.GetValueOrDefault(isRemembered, booleanDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isRemembered, value);
            }
        }


    }


}
