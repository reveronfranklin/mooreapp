﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class CantidadOrdenesDiaiasResult
    {

        public string Dia { get; set; }

        public int CantidadOrdenes { get; set; }

        public decimal Venta { get; set; }

    }
}
