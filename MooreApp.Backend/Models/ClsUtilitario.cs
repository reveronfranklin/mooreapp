﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using MooreApp.Common.Models;
using System.ComponentModel.DataAnnotations;
using MooreApp.Domain.Models;
using System.Data.Entity;

namespace MooreApp.Common.Models
{
 
        public class ClsUtilitario
        {

         private DataContext db = new DataContext();

         const string Comillas = "\"";

         
        
        public bool TicketValido(string sha1)
        {

        SegTicket Ticket = db.SegTicket.Find(sha1);

        if (Ticket == null)
        {
            return false;
        }
        else
        {
            if (Ticket.FlagUsado == false)
            {

                db.Entry(Ticket).State = EntityState.Modified; ;

                Ticket.FlagUsado = true;

                Ticket.FechaHoraUso = DateTime.Now;

                db.SaveChanges();

                return true;

            }
            if (Ticket.FlagUsado == true && Ticket.FechaHoraGetIp == null)
            {
                return true;
            }
            if (Ticket.FechaHoraGetIp != null)
            {
                return false;
            }

            return false;

        }

        }

        public static string ConvertSha1(string str)
        {
        System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1Managed.Create();
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        byte[] stream = null;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        stream = sha1.ComputeHash(encoding.GetBytes(str));
        for (int i = 0; i <= (stream.Length - 1); i++)
        {
            sb.AppendFormat("{0:x2}", stream[i]);
        }
        return sb.ToString();
        }

       
        public enum TipoAcciones { Crear = 1, Modificar = 2, Eliminar = 3 };

        public enum TipoMensajes { Exitoso = 1, Advertencia = 2, Error = 3 };

        public string Mensaje(string TextoMensaje, TipoMensajes TipoMensaje)
        {
        string CerrarAlerta = "<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Cerrar</span></button>";

        switch (TipoMensaje)
        {
            case TipoMensajes.Exitoso:
                return "<div class= " + Comillas + "form-group alert alert-info alert-dismissible" + Comillas + ">" + TextoMensaje + CerrarAlerta + "<div/>";
            case TipoMensajes.Advertencia:
                return "<div class= " + Comillas + "form-group alert alert-warning alert-dismissible" + Comillas + ">" + TextoMensaje + CerrarAlerta + "<div/>";
            case TipoMensajes.Error:
                return "<div class= " + Comillas + "form-group alert alert-danger alert-dismissible" + Comillas + ">" + TextoMensaje + CerrarAlerta + "<div/>";
            default:
                return "<div>" + TextoMensaje + CerrarAlerta + "<div/>";
        }

        }

     

    }
  
}
