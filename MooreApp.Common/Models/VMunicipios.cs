﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class VMunicipios
    {
        [Key]
        public decimal RECNUM { get; set; }
        public string CODIGO_ESTADO { get; set; }
        public string CODIGO_MCPO { get; set; }
        public string DESC_MUNICIPIO { get; set; }
        public string CAPITAL_MCPO { get; set; }
        public int ZONA_DESPACHO { get; set; }
        public int POSICION_RUTA { get; set; }
        public string EDO_MCPO { get; set; }
        public string CODIGO_JDE { get; set; }
        public int KILOMETROS { get; set; }
        public int IdOficinaFisica { get; set; }
        public int IdGrupoOficina { get; set; }
        public int PorcFlete { get; set; }
        public string NOMBRE_ESTADO { get; set; }
  
    }
}
