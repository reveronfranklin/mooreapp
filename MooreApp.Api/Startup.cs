﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Owin;
using MooreApp.Domain.Models;
using Owin;

[assembly: OwinStartup(typeof(MooreApp.Api.Startup))]
namespace MooreApp.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Database.SetInitializer<DataContext>(null);
        }
    }
}
