﻿


namespace MooreApp.Backend.Models
{

    using MooreApp.Domain.Models;
    public class LocalDataContext:DataContext
    {

        public System.Data.Entity.DbSet<MooreApp.Common.Models.PreciosStockEstadoMunicipios> PreciosStockEstadoMunicipios { get; set; }
     
    }
}