﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Backend.Controllers
{
    public class AppBacklogInicialsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AppBacklogInicials
        public async Task<ActionResult> Index()
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            return View(await db.AppBacklogInicial.ToListAsync());
        }

        // GET: AppBacklogInicials/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppBacklogInicial appBacklogInicial = await db.AppBacklogInicial.FindAsync(id);
            if (appBacklogInicial == null)
            {
                return HttpNotFound();
            }
            return View(appBacklogInicial);
        }

        // GET: AppBacklogInicials/Create
        public ActionResult Create()
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        // POST: AppBacklogInicials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AppBacklogInicial appBacklogInicial)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (ModelState.IsValid)
            {
                db.AppBacklogInicial.Add(appBacklogInicial);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(appBacklogInicial);
        }

        // GET: AppBacklogInicials/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppBacklogInicial appBacklogInicial = await db.AppBacklogInicial.FindAsync(id);
            if (appBacklogInicial == null)
            {
                return HttpNotFound();
            }
            return View(appBacklogInicial);
        }

        // POST: AppBacklogInicials/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AppBacklogInicial appBacklogInicial)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (ModelState.IsValid)
            {
                db.Entry(appBacklogInicial).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(appBacklogInicial);
        }

        // GET: AppBacklogInicials/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppBacklogInicial appBacklogInicial = await db.AppBacklogInicial.FindAsync(id);
            if (appBacklogInicial == null)
            {
                return HttpNotFound();
            }
            return View(appBacklogInicial);
        }

        // POST: AppBacklogInicials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            AppBacklogInicial appBacklogInicial = await db.AppBacklogInicial.FindAsync(id);
            db.AppBacklogInicial.Remove(appBacklogInicial);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
