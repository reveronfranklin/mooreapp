﻿using System.ComponentModel.DataAnnotations;

namespace MooreApp.Common.Models
{
    public class AppUsuarioPages
    {
        [Key]
        public int Id { get; set; }
        public string  Usuario { get; set; }
        public string Page { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }

    }
}
