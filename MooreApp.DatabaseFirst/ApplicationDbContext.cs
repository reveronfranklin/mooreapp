namespace MooreApp.DatabaseFirst
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("name=ApplicationDbContext")
        {
        }

        public virtual DbSet<WSMY501> WSMY501 { get; set; }
        public virtual DbSet<WSMY502> WSMY502 { get; set; }
        public virtual DbSet<WSMY503> WSMY503 { get; set; }
        public virtual DbSet<WSMY515> WSMY515 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WSMY501>()
                .Property(e => e.Id_direccion)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY501>()
                .Property(e => e.Id_direccionEntregar)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY501>()
                .Property(e => e.idsolicitudCredito)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Cod_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Unidad_Cotizacion)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Observaciones)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Cotizacion_Destino)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.ObservacionesGanarPerder)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Total_Renglon)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Total_Ganado)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.LoteAct)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Id)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.RTotal_Renglon)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.RTotal_Ganado)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Total_RenglonUsd)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY502>()
                .Property(e => e.Total_GanadoUsd)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Ganada)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Modificar)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Activa)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Postergada)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Perdida)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_Caducada)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_EnEspera)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Flag_CaducaInactiva)
                .IsFixedLength();

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.Abreviado)
                .IsUnicode(false);

            modelBuilder.Entity<WSMY503>()
                .Property(e => e.CondicionRazonId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.Cantidad)
                .HasPrecision(16, 3);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PrecioUnitario)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalPropuesta)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorMC)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.BsMC)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.LoteAct)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.Cant_x_Caja)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.Cajas)
                .HasPrecision(18, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.Cant_Formas)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.Id)
                .HasPrecision(18, 0);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcRespSocial)
                .HasPrecision(5, 3);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcComRegulada)
                .HasPrecision(5, 3);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcTolerancia)
                .HasPrecision(16, 2);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorMcFinan)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcMcMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorMcBruto)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcComision)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.FactorSdf)
                .HasPrecision(16, 2);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.BsListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalBsListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcLista)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.BsListaCPJMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalBsListaCPJMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PrecioEtiqueta)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PorcIva)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RPrecioUnitario)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RTotalPropuesta)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RBsMC)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RBsListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RTotalBsListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RBsListaCPJMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RTotalBsListaCPJMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.RPrecioEtiqueta)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.PrecioUnitarioUsd)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalPropuestaUsd)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.UsdListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalUsdListaCPJ)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.UsdListaCPJMinimo)
                .HasPrecision(20, 4);

            modelBuilder.Entity<WSMY515>()
                .Property(e => e.TotalUsdListaCPJMinimo)
                .HasPrecision(20, 4);
        }
    }
}
