﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public partial class SegModulo
    {
        [Key]
        public int IdModulo { get; set; }
        public string NombreModulo { get; set; }
        public string LinkModulo { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> IndiceModulo { get; set; }
        public Nullable<int> IdPrograma { get; set; }
        public Nullable<int> IdModuloPadre { get; set; }
        public Nullable<short> TipoLLamada { get; set; }
        public string Icono { get; set; }
        public Nullable<short> ServidorReporte { get; set; }
        public string RutaReporte { get; set; }
        public string NombreReporte { get; set; }
        public Nullable<short> IdTipoReporte { get; set; }
        public string FlagPortafolioReportes { get; set; }
        public string IconoMenu { get; set; }
    }
}
