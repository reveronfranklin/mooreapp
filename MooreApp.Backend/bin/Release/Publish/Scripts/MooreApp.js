﻿$("#Estado").change(function () {
    $("#Municipio").empty();
    document.getElementById('Precio').value = '';
    $.ajax({
        type: 'POST',
        url: Url,
        dataType: 'json',
        data: { estado: $("#Estado").val() },
        success: function (municipios) {
            $.each(municipios, function (i, municipio) {
                $("#Municipio").append('<option value="'
                    + municipio.CODIGO_MCPO + '">'
                    + municipio.CAPITAL_MCPO + '</option>');
            });
        },
        error: function (ex) {
            alert('Failed to retrieve Municipios.' + ex);
        }
    });

    return false;
});

$("#Municipio").change(function () {
  
    
    document.getElementById('Precio').value = '';

    return false;
});