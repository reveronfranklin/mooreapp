﻿using MooreApp.Common.Models;
using MooreApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MooreApp.Api.Controllers
{
    public class CotizacionController : ApiController
    {
        private DataContext db = new DataContext();


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpGet,
        System.Web.Http.Route("api/Cotizacion/Get/{id}")]
        public IHttpActionResult Get(string id)
        {
            CotizacionResponse result = new CotizacionResponse();
            List<DetalleCotizacionResponse> DetalleCotizacionresult = new List<DetalleCotizacionResponse>();

            try
            {


                var cotizacion = db.WSMY501.Where(p => p.Cotizacion == id).FirstOrDefault();

                if (cotizacion == null)
                {
                    return null;
                }
                else
                {
                    result.Cotizacion = cotizacion.Cotizacion;
                    result.Cod_Vendedor = cotizacion.Cod_Vendedor;
                    result.Cod_Cliente = cotizacion.Cod_Cliente;
                    result.Razon_Social = cotizacion.Razon_Social;
                    result.FechaPago = cotizacion.FechaPago;
                    result.FijarPrecioBs = cotizacion.FijarPrecioBs;
                    result.FijarPrecioBsAprobado = cotizacion.FijarPrecioBsAprobado;
                    result.ObservacionPago = cotizacion.ObservacionPago;

                    var vendedor = db.PCVendedor.Where(v => v.IdVendedor == cotizacion.Cod_Vendedor).FirstOrDefault();

                    if (vendedor != null)
                    {
                        result.NombreVendedor = vendedor.Nombre;
                    }

                    var detalle = db.WSMY515.Where(c => c.Cotizacion == id).ToList();
                    foreach (var item in detalle)
                    {
                        var wsmy502 = db.WSMY502.Where(r => r.Cotizacion == item.Cotizacion && r.Renglon == item.Renglon).FirstOrDefault();

                        var wsmy503 = db.WSMY503.Where(r => r.Codigo == item.Estatus).FirstOrDefault();

                        DetalleCotizacionResponse detalleNew = new DetalleCotizacionResponse();

                        detalleNew.Cotizacion = item.Cotizacion;
                        detalleNew.FlagEstimada = wsmy502.FlagEstimada;
                        detalleNew.Cod_Producto = wsmy502.Cod_Producto;
                        detalleNew.DescripcionStatus = wsmy503.Descripcion;
                        detalleNew.TotalPropuesta = item.TotalPropuesta;
                        detalleNew.TotalPropuestaUsd = item.TotalPropuestaUsd;
                        DetalleCotizacionresult.Add(detalleNew);

                    }
                    result.DetalleCotizacionResponse = DetalleCotizacionresult;

                }

                // return result;
                return Ok(new { CotizacionResponse = result });


            }
            catch (Exception)
            {
                return null;



            }



        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost,
        System.Web.Http.Route("api/Cotizacion/GetCotizacion/")]
        public IHttpActionResult GetCotizacion(CredentialsViewModel vm)
        {
            CotizacionResponse result = new CotizacionResponse( );
            List<DetalleCotizacionResponse> DetalleCotizacionresult = new List<DetalleCotizacionResponse>();
          
            try
            {
                             

                var cotizacion =  db.WSMY501.Where(p => p.Cotizacion == vm.id).FirstOrDefault();

                if (cotizacion==null)
                {
                    return null;
                }
                else
                {
                    result.Cotizacion = cotizacion.Cotizacion;
                    result.Cod_Vendedor = cotizacion.Cod_Vendedor;
                    result.Cod_Cliente = cotizacion.Cod_Cliente;
                    result.Razon_Social = cotizacion.Razon_Social;
                    result.FechaPago = cotizacion.FechaPago;
                    result.FijarPrecioBs = cotizacion.FijarPrecioBs;
                    result.FijarPrecioBsAprobado = cotizacion.FijarPrecioBsAprobado;

                    if (cotizacion.FijarPrecioBs==true)
                    {
                        result.FijarPrecioBsTexto = "SI";
                    }
                    else
                    {
                        result.FijarPrecioBsTexto = "NO";
                    }

                    if (cotizacion.FijarPrecioBsAprobado == true)
                    {
                        result.FijarPrecioBsAprobadoTexto = "SI";
                    }
                    else
                    {
                        result.FijarPrecioBsAprobadoTexto = "NO";
                    }


                    result.ObservacionPago = cotizacion.ObservacionPago;

                    var vendedor = db.PCVendedor.Where(v => v.IdVendedor == cotizacion.Cod_Vendedor).FirstOrDefault();

                    if (vendedor!=null)
                    {
                        result.NombreVendedor = vendedor.Nombre;
                    }
                   
                    var detalle = db.WSMY515.Where(c => c.Cotizacion == vm.id).ToList();
                    foreach (var item in detalle)
                    {
                        var wsmy502 = db.WSMY502.Where(r => r.Cotizacion == item.Cotizacion && r.Renglon == item.Renglon).FirstOrDefault();

                        var wsmy503 = db.WSMY503.Where(r => r.Codigo == item.Estatus).FirstOrDefault();

                        DetalleCotizacionResponse detalleNew = new DetalleCotizacionResponse();

                        detalleNew.Cotizacion = item.Cotizacion;
                        detalleNew.FlagEstimada = wsmy502.FlagEstimada;
                        if (wsmy502.FlagEstimada==true)
                        {
                            detalleNew.FlagEstimadaTexto = "SI";

                        }
                        else
                        {
                            detalleNew.FlagEstimadaTexto = "no";
                        }
                        detalleNew.Cod_Producto = wsmy502.Cod_Producto;
                        detalleNew.DescripcionStatus = wsmy503.Descripcion;
                        detalleNew.TotalPropuesta=item.TotalPropuesta;
                        detalleNew.TotalPropuestaUsd = item.TotalPropuestaUsd;
                        DetalleCotizacionresult.Add(detalleNew);

                    }
                    result.DetalleCotizacionResponse = DetalleCotizacionresult;

                }

               // return result;
                return Ok(new { CotizacionResponse = result });


            }
            catch (Exception)
            {
                return null;



            }



        }

        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost,
      System.Web.Http.Route("api/Cotizacion/UpdateCotizacion/")]
        public CotizacionResponse UpdateCotizacion(CotizacionResponse vm)
        {
            CotizacionResponse result = new CotizacionResponse();
            List<DetalleCotizacionResponse> DetalleCotizacionresult = new List<DetalleCotizacionResponse>();

            try
            {


                var cotizacion = db.WSMY501.Where(p => p.Cotizacion == vm.Cotizacion).FirstOrDefault();

                if (cotizacion == null)
                {
                    return null;
                }
                else
                {

                    cotizacion.FechaPago = vm.FechaPago;
                    cotizacion.ObservacionPago = vm.ObservacionPago;
                    db.Entry(cotizacion).State = EntityState.Modified;
                    db.SaveChanges();

                }


                return vm;



            }
            catch (Exception)
            {
                return null;



            }



        }

    }
}
