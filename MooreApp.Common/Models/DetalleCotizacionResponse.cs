﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class DetalleCotizacionResponse
    {
        [StringLength(13)]
        public string Cotizacion { get; set; }

        [StringLength(50)]
        public string Cod_Producto { get; set; }



        [Column(TypeName = "numeric")]
        public decimal? TotalPropuesta { get; set; }


        [Column(TypeName = "numeric")]
        public decimal? TotalPropuestaUsd { get; set; }

        [StringLength(100)]
        public string DescripcionStatus { get; set; }

        public bool? FlagEstimada { get; set; }

        public string FlagEstimadaTexto { get; set; }

    }
}
