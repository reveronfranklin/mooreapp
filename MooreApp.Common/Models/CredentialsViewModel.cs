﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class CredentialsViewModel
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public bool Validate { get; set; }
        public string GuidUser { get; set; }
        public string GuidCliente { get; set; }
        public string id { get; set; }
        public string Role { get; set; }
    }
}
