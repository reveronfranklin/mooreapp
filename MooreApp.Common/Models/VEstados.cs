﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class VEstados
    {
        [Key]
        public decimal RECNUM { get; set; }
        public string CODIGO_ESTADO { get; set; }
        public string NOMBRE_ESTADO { get; set; }
        public string CAPITAL_ESTADO { get; set; }
        public string CODIGO_JDE { get; set; }
        public string CODIGO_SAP { get; set; }
    


    }
}
