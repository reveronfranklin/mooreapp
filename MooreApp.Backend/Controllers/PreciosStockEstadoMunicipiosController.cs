﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Backend.Models;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Backend.Controllers
{
    public class PreciosStockEstadoMunicipiosController : Controller
    {
        private DataContext db = new DataContext();

        // GET: PreciosStockEstadoMunicipios
        public async Task<ActionResult> Index()
        {
            return View(await db.PreciosStockEstadoMunicipios.ToListAsync());
        }

        // GET: PreciosStockEstadoMunicipios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return HttpNotFound();
            }
            return View(preciosStockEstadoMunicipio);
        }

        // GET: PreciosStockEstadoMunicipios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PreciosStockEstadoMunicipios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Producto,Estado,Municipio,Descripcion,NombreEstado,NombreMunicipio,CapitalMunicipio,PrecioMinimo,PrecioMaximo,IdCalculo,FechaCalculo")] PreciosStockEstadoMunicipios preciosStockEstadoMunicipio)
        {
            if (ModelState.IsValid)
            {
                db.PreciosStockEstadoMunicipios.Add(preciosStockEstadoMunicipio);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(preciosStockEstadoMunicipio);
        }

        // GET: PreciosStockEstadoMunicipios/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return HttpNotFound();
            }
            return View(preciosStockEstadoMunicipio);
        }

        // POST: PreciosStockEstadoMunicipios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Producto,Estado,Municipio,Descripcion,NombreEstado,NombreMunicipio,CapitalMunicipio,PrecioMinimo,PrecioMaximo,IdCalculo,FechaCalculo")] PreciosStockEstadoMunicipios preciosStockEstadoMunicipio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(preciosStockEstadoMunicipio).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(preciosStockEstadoMunicipio);
        }

        // GET: PreciosStockEstadoMunicipios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return HttpNotFound();
            }
            return View(preciosStockEstadoMunicipio);
        }

        // POST: PreciosStockEstadoMunicipios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            db.PreciosStockEstadoMunicipios.Remove(preciosStockEstadoMunicipio);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
