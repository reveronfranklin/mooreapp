namespace MooreApp.DatabaseFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WSMY502
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(13)]
        public string Cotizacion { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Renglon { get; set; }

        [StringLength(50)]
        public string Cod_Producto { get; set; }

        [StringLength(200)]
        public string Descripcion { get; set; }

        public int? Estatus { get; set; }

        public int? RazonGanadaPerdida { get; set; }

        public int? Competidor { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Fecha_Entrega { get; set; }

        [StringLength(5)]
        public string Unidad_Cotizacion { get; set; }

        [StringLength(300)]
        public string Observaciones { get; set; }

        [StringLength(100)]
        public string Cotizacion_Destino { get; set; }

        public int? SubCategoria { get; set; }

        public int? Probabilidad { get; set; }

        public int? TiempoEntrega { get; set; }

        [StringLength(300)]
        public string ObservacionesGanarPerder { get; set; }

        public DateTime? FechaRegistro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Total_Renglon { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Total_Ganado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LoteAct { get; set; }

        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal Id { get; set; }

        public DateTime? FechaReorden { get; set; }

        [StringLength(4000)]
        public string Combinada { get; set; }

        public short? IdReferenciaProducto { get; set; }

        public short? IdTituloCalendario { get; set; }

        [StringLength(4)]
        public string EstatusPlanta { get; set; }

        [StringLength(1)]
        public string FlagActualizado { get; set; }

        [StringLength(1)]
        public string FlagImpContrato { get; set; }

        [StringLength(1000)]
        public string AlertaCalidad { get; set; }

        public bool? FlagEstimada { get; set; }

        public bool? FlagPortadiseno { get; set; }

        public bool FlagCombinada { get; set; }

        public long IdCombinada { get; set; }

        public long Orden { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RTotal_Renglon { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RTotal_Ganado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Total_RenglonUsd { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Total_GanadoUsd { get; set; }

        public virtual WSMY501 WSMY501 { get; set; }
    }
}
