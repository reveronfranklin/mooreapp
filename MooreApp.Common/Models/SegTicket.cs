﻿

namespace MooreApp.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class SegTicket
    {
        public decimal Id { get; set; }
        [Key]
        public string TicketSha1 { get; set; }
        public bool FlagUsado { get; set; }
        public string IdUsuario { get; set; }
        public string IpUsuario { get; set; }
        public System.DateTime FechaHoraCreacion { get; set; }
        public Nullable<System.DateTime> FechaHoraUso { get; set; }
        public Nullable<System.DateTime> FechaHoraGetIp { get; set; }
    }
}
