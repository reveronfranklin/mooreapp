﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace MooreApp.Common.Models
{
    public class WITY021
    {

        [Key]
        public int IDUsuario { get; set; }

      
        public string NombreUsuario { get; set; }

       
        public string Usuario { get; set; }


        public string Clave { get; set; }

        public string Inactivo { get; set; }

        public DateTime FECHA_CAMBIO { get; set; }

        public int CONTEO_EXPIRA { get; set; }

        public decimal INTENTOSFALLIDOS { get; set; }

        public int IDOFICINA { get; set; }

        public string Email { get; set; }


        public string FICHA { get; set; }


        public double AN8 { get; set; }


        public string UsuarioDominio { get; set; }


        public string FlagPersonalSistemas { get; set; }



        public string FlagAutomata { get; set; }


        public string ClaveOculta { get; set; }


        public string Telefono { get; set; }


        public string ClasificacionUsuario { get; set; }


        public string Imei1 { get; set; }


        public string EquipoImei1 { get; set; }


        public string Imei2 { get; set; }


        public string EquipoImei2 { get; set; }


        public string Imei3 { get; set; }


        public string EquipoImei3 { get; set; }

     
    }
}
