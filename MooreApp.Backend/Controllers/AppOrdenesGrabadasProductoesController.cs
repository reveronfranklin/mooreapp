﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Backend.Controllers
{
    public class AppOrdenesGrabadasProductoesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AppOrdenesGrabadasProductoes
        public async Task<ActionResult> Index()
        {

            using (var db = new DataContext())
            {


                bool Actualizado = false;
                AppOrdenesGrabadasProducto ordenes  = new AppOrdenesGrabadasProducto();
                ordenes = db.AppOrdenesGrabadasProducto.Where(o => o.DiaChar == "").FirstOrDefault();
                if (ordenes==null)
                {
                    Actualizado = true;
                }
                if (!Actualizado)
                {
                    List<AppOrdenesGrabadasProducto> diarias = new List<AppOrdenesGrabadasProducto>();
                    diarias = db.AppOrdenesGrabadasProducto.OrderBy(d => d.Dia).ToList();
                    if (diarias != null)
                    {

                        foreach (var item in diarias)
                        {
                            if (item.DiaChar != "TOTAL")
                            {
                                item.DiaChar = item.Dia.Day.ToString() + "-" + GetMonthChar(item.Dia.Month);
                                db.Entry(item).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                            }


                        }
                    }
                }
               
            }





            return View(await db.AppOrdenesGrabadasProducto.ToListAsync());
        }

        // GET: AppOrdenesGrabadasProductoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppOrdenesGrabadasProducto appOrdenesGrabadasProducto = await db.AppOrdenesGrabadasProducto.FindAsync(id);
            if (appOrdenesGrabadasProducto == null)
            {
                return HttpNotFound();
            }
            return View(appOrdenesGrabadasProducto);
        }

        // GET: AppOrdenesGrabadasProductoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AppOrdenesGrabadasProductoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Año,Mes,Dia,DiaChar,CantFormas,BsFormas,CantEtiquetas,BsEtiquetas,CantICPP,BsICPP,CantCustomPrint,BsCustomPrint,CantOfficeProduct,BsOfficeProduct,CantTotal,BsTotal")] AppOrdenesGrabadasProducto appOrdenesGrabadasProducto)
        {
            if (ModelState.IsValid)
            {
                db.AppOrdenesGrabadasProducto.Add(appOrdenesGrabadasProducto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(appOrdenesGrabadasProducto);
        }

        // GET: AppOrdenesGrabadasProductoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppOrdenesGrabadasProducto appOrdenesGrabadasProducto = await db.AppOrdenesGrabadasProducto.FindAsync(id);
            if (appOrdenesGrabadasProducto == null)
            {
                return HttpNotFound();
            }
            return View(appOrdenesGrabadasProducto);
        }

        // POST: AppOrdenesGrabadasProductoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Año,Mes,Dia,DiaChar,CantFormas,BsFormas,CantEtiquetas,BsEtiquetas,CantICPP,BsICPP,CantCustomPrint,BsCustomPrint,CantOfficeProduct,BsOfficeProduct,CantTotal,BsTotal")] AppOrdenesGrabadasProducto appOrdenesGrabadasProducto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appOrdenesGrabadasProducto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(appOrdenesGrabadasProducto);
        }

        // GET: AppOrdenesGrabadasProductoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppOrdenesGrabadasProducto appOrdenesGrabadasProducto = await db.AppOrdenesGrabadasProducto.FindAsync(id);
            if (appOrdenesGrabadasProducto == null)
            {
                return HttpNotFound();
            }
            return View(appOrdenesGrabadasProducto);
        }

        // POST: AppOrdenesGrabadasProductoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AppOrdenesGrabadasProducto appOrdenesGrabadasProducto = await db.AppOrdenesGrabadasProducto.FindAsync(id);
            db.AppOrdenesGrabadasProducto.Remove(appOrdenesGrabadasProducto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }



        public string GetMonthChar(int month)
        {
            string result = "";

            switch (month)
            {
                case 1:
                    result = "Ene";
                    break;
                case 2:
                    result = "Feb";
                    break;
                case 3:
                    result = "Mar";
                    break;
                case 4:
                    result = "Abr";
                    break;
                case 5:
                    result = "May";
                    break;
                case 6:
                    result = "Jun";
                    break;
                case 7:
                    result = "Jul";
                    break;
                case 8:
                    result = "Ago";
                    break;
                case 9:
                    result = "Sep";
                    break;
                case 10:
                    result = "oct";
                    break;
                case 11:
                    result = "Nov";
                    break;
                case 12:
                    result = "Dic";
                    break;

                default:

                    break;
            }


            return result;

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
