﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;
using System.Globalization;

namespace MooreApp.Backend.Controllers
{
   
    public class V_ProductosController : Controller
    {
        List<Estado> estado;
        List<Municipio> municipio;
        private DataContext db = new DataContext();



        // GET: V_Productos
        public async Task<ActionResult> Index()
        {
            if (Session["IdRol"]==null)
            {
                return RedirectToAction("Login","User");
            }
            

            return View(await db.V_Productos.ToListAsync());
        }
        // GET: V_Productos/DetailPrice/RESMAC97
        public async Task<ActionResult> DetailPrice(string id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            initEstado();
            ViewBag.Estados = new SelectList(db.VEstados, "CODIGO_ESTADO", "NOMBRE_ESTADO");
            //ViewBag.Municipios = new SelectList(municipio, "CodMunicipio", "Nombre");

            var precios = await  db.PreciosStockEstadoMunicipios.Where(p => p.Producto == id).ToListAsync();
            return View(precios);
        }
        void initEstado()
        {
            estado = new List<Estado>();
            estado.Add(new Estado { CodEstado = "02", Nombre = "AMAZONAS" });
            estado.Add(new Estado { CodEstado = "03", Nombre = "ANZOATEGUI" });
            estado.Add(new Estado { CodEstado = "04", Nombre = "APURE" });
            estado.Add(new Estado { CodEstado = "05", Nombre = "ARAGUA" });
            estado.Add(new Estado { CodEstado = "06", Nombre = "BARINAS" });
            estado.Add(new Estado { CodEstado = "07", Nombre = "BOLIVAR" });
            estado.Add(new Estado { CodEstado = "08", Nombre = "CARABOBO" });
            estado.Add(new Estado { CodEstado = "09", Nombre = "COJEDES" });
            estado.Add(new Estado { CodEstado = "10", Nombre = "DELTA AMACURO" });
            estado.Add(new Estado { CodEstado = "01", Nombre = "DISTRITO CAPITAL" });
            estado.Add(new Estado { CodEstado = "11", Nombre = "FALCON" });
            estado.Add(new Estado { CodEstado = "12", Nombre = "GUARICO" });
            estado.Add(new Estado { CodEstado = "13", Nombre = "LARA" });
            estado.Add(new Estado { CodEstado = "14", Nombre = "MERIDA" });
            estado.Add(new Estado { CodEstado = "15", Nombre = "MIRANDA" });
            estado.Add(new Estado { CodEstado = "16", Nombre = "MONAGAS" });
            estado.Add(new Estado { CodEstado = "25", Nombre = "NUEVA ESPARTA" });
            estado.Add(new Estado { CodEstado = "18", Nombre = "PORTUGUESA" });
            estado.Add(new Estado { CodEstado = "19", Nombre = "SUCRE" });
            estado.Add(new Estado { CodEstado = "20", Nombre = "TACHIRA" });
            estado.Add(new Estado { CodEstado = "21", Nombre = "TRUJILLO" });
            estado.Add(new Estado { CodEstado = "24", Nombre = "VARGAS" });
            estado.Add(new Estado { CodEstado = "22", Nombre = "YARACUY" });
            estado.Add(new Estado { CodEstado = "23", Nombre = "ZULIA" });


            //PickerEstado.ItemsSource = estado.OrderBy(e => e.Nombre).ToList();
            //PickerEstado.Title = "Seleccione Estado";


        }
        void initMunicipio()
        {
            municipio = new List<Municipio>();
            //DISTRITO CAPITAL
            municipio.Add(new Municipio { CodEstado = "01", CodMunicipio = "01", Nombre = "CARACAS" });

            //02 AMAZONAS
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "01", Nombre = "LA ESMERALDA" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "02", Nombre = "SAN FERNANDO DE ATABAPO" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "03", Nombre = "PUERTO AYACUCHO" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "04", Nombre = "ISLA RATON" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "05", Nombre = "MAROA" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "06", Nombre = "SAN JUAN DE MANAPIARE" });
            municipio.Add(new Municipio { CodEstado = "02", CodMunicipio = "07", Nombre = "SAN CARLOS DE RIO NEGRO" });
            //03 ANZOATEGUI
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "01", Nombre = "ANACO" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "02", Nombre = "ARAGUA DE BARCELONA" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "03", Nombre = "PUERTO PIRITU" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "04", Nombre = "VALLE DE GUANAPE" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "05", Nombre = "PARIAGUAN" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "06", Nombre = "GUANTA" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "07", Nombre = "SOLEDAD" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "08", Nombre = "PUERTO LA CRUZ" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "09", Nombre = "ONOTO" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "10", Nombre = "MAPIRE" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "11", Nombre = "SAN MATEO" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "12", Nombre = "CLARINES" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "13", Nombre = "CANTAURA" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "14", Nombre = "PIRITU" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "15", Nombre = "SAN JOSE DE GUANIPA(El Tigrito)" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "16", Nombre = "BOCA DE UCHIRE" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "17", Nombre = "SANTA ANA" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "18", Nombre = "BARCELONA" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "19", Nombre = "El TIGRE" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "20", Nombre = "EL CHAPARRO" });
            municipio.Add(new Municipio { CodEstado = "03", CodMunicipio = "21", Nombre = "LECHERIAS" });

            //04 APURE
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "01", Nombre = "ACHAGUAS" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "02", Nombre = "BIRUACA" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "03", Nombre = "BRUZUAL" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "04", Nombre = "GUASDUALITO" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "05", Nombre = "SAN JUAN DE PAYARA" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "06", Nombre = "ELORZA" });
            municipio.Add(new Municipio { CodEstado = "04", CodMunicipio = "07", Nombre = "SAN FERNANDO DE APURE" });

            // 05 ARAGUA
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "01", Nombre = "SAN MATEO" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "02", Nombre = "CAMATAGUA" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "03", Nombre = "MARACAY" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "04", Nombre = "SANTA CRUZ" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "05", Nombre = "LA VICTORIA" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "06", Nombre = "EL CONSEJO" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "07", Nombre = "PALO NEGRO" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "08", Nombre = "EL LIMON" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "09", Nombre = "SAN CASIMIRO" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "10", Nombre = "SAN SEBASTIAN" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "11", Nombre = "TURMERO" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "12", Nombre = "LAS TEJERIAS" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "13", Nombre = "CAGUA" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "14", Nombre = "LA COLONIA TOVAR" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "15", Nombre = "BARBACOAS" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "16", Nombre = "VILLA DE CURA" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "17", Nombre = "SANTA RITA" });
            municipio.Add(new Municipio { CodEstado = "05", CodMunicipio = "18", Nombre = "OCUMARE DE LA COSTA" });

            //06 BARINAS
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "01", Nombre = "SABANETA" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "02", Nombre = "SOCOPO" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "03", Nombre = "ARISMENDI" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "04", Nombre = "BARINAS" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "05", Nombre = "BARINITAS" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "06", Nombre = "BARRANCAS" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "07", Nombre = "SANTA BARBARA" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "08", Nombre = "OBISPOS" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "09", Nombre = "CIUDAD BOLIVIA" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "10", Nombre = "LIBERTAD" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "11", Nombre = "CIUDAD DE NUTRIAS" });
            municipio.Add(new Municipio { CodEstado = "06", CodMunicipio = "12", Nombre = "EL CANTON" });

            //07 BOLIVAR
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "01", Nombre = "CIUDAD GUAYANA" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "02", Nombre = "CAICARA DEL ORINOCO" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "03", Nombre = "EL CALLAO" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "04", Nombre = "SANTA ELENA DE UAIREN" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "05", Nombre = "CIUDAD BOLIVAR" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "06", Nombre = "UPATA" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "07", Nombre = "CIUDAD PIAR" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "08", Nombre = "GUASIPATI" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "09", Nombre = "TUMEREMO" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "10", Nombre = "MARIPA" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "11", Nombre = "EL PALMAR" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "12", Nombre = "PUERTO ORDAZ" });
            municipio.Add(new Municipio { CodEstado = "07", CodMunicipio = "13", Nombre = "SAN FELIX" });
            //08 CARABOBO
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "01", Nombre = "BEJUMA" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "02", Nombre = "GUIGUE" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "03", Nombre = "MARIARA" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "04", Nombre = "GUACARA" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "05", Nombre = "MORON" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "06", Nombre = "TOCUYITO" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "07", Nombre = "LOS GUAYOS" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "08", Nombre = "MIRANDA" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "09", Nombre = "MONTALBAN" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "10", Nombre = "NAGUANAGUA" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "11", Nombre = "PUERTO CABELLO" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "12", Nombre = "SAN DIEGO" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "13", Nombre = "SAN JOAQUIN" });
            municipio.Add(new Municipio { CodEstado = "08", CodMunicipio = "14", Nombre = "VALENCIA" });

            //09 COJEDES
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "01", Nombre = "COJEDES" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "02", Nombre = "TINAQUILLO" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "03", Nombre = "EL BAUL" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "04", Nombre = "MACAPO" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "05", Nombre = "EL PAO" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "06", Nombre = "LIBERTAD" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "07", Nombre = "LAS VEGAS" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "08", Nombre = "SAN CARLOS" });
            municipio.Add(new Municipio { CodEstado = "09", CodMunicipio = "09", Nombre = "TINACO" });

            //10 DELTA AMACURO
            municipio.Add(new Municipio { CodEstado = "10", CodMunicipio = "01", Nombre = "CURIAPO" });
            municipio.Add(new Municipio { CodEstado = "10", CodMunicipio = "02", Nombre = "SIERRA IMATACA" });
            municipio.Add(new Municipio { CodEstado = "10", CodMunicipio = "03", Nombre = "PEDERNALES" });
            municipio.Add(new Municipio { CodEstado = "10", CodMunicipio = "04", Nombre = "TUCUPITA" });

            //11 FALCON
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "01", Nombre = "SAN JUAN  DE LOS CAYOS" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "02", Nombre = "SAN LUIS" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "03", Nombre = "CAPATARIDA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "04", Nombre = "YARACAL" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "05", Nombre = "PUNTO FIJO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "06", Nombre = "LA VELA DE CORO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "07", Nombre = "DABAJURO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "08", Nombre = "PEDREGAL" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "09", Nombre = "PUEBLO NUEVO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "10", Nombre = "CHURUGUARA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "11", Nombre = "JACURA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "12", Nombre = "SANTA CRUZ DE LOS TAQUES" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "13", Nombre = "MENE DE MAUROA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "14", Nombre = "SANTA ANA DE CORO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "15", Nombre = "CHICHIRIVICHE" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "16", Nombre = "PALMASOLA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "17", Nombre = "CABURE" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "18", Nombre = "PIRITU" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "19", Nombre = "MIRIMIRE" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "20", Nombre = "TUCACAS" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "21", Nombre = "LA CRUZ DE TARATARA" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "22", Nombre = "TOCOPERO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "23", Nombre = "SANTA CRUZ DE BUCARAL" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "24", Nombre = "URUMACO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "25", Nombre = "PUERTO CUMAREBO" });
            municipio.Add(new Municipio { CodEstado = "11", CodMunicipio = "26", Nombre = "CORO" });

            // 12 GUARICO
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "01", Nombre = "CAMAGUAN" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "02", Nombre = "CHAGUARAMAS" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "03", Nombre = "EL SOCORRO" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "04", Nombre = "GUAYABAL" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "05", Nombre = "VALLE DE LA PASCUA" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "06", Nombre = "LAS MERCEDES" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "07", Nombre = "EL SOMBRERO" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "08", Nombre = "CALABOZO" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "09", Nombre = "ALTAGRACIA DE ORITUCO" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "10", Nombre = "ORTIZ" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "11", Nombre = "TUCUPIDO" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "12", Nombre = "SAN JUAN DE LOS MORROS" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "13", Nombre = "SAN JOSE DE GUARIBE" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "14", Nombre = "SANTA MARIA DE IPIRE" });
            municipio.Add(new Municipio { CodEstado = "12", CodMunicipio = "15", Nombre = "ZARAZA" });

            //13 LARA
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "01", Nombre = "SANARE" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "02", Nombre = "DUACA" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "03", Nombre = "BARQUISIMETO" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "04", Nombre = "QUIBOR" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "05", Nombre = "EL TOCUYO" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "06", Nombre = "CABUDARE" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "07", Nombre = "SARARE" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "08", Nombre = "CARORA" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "09", Nombre = "SIQUISIQUE" });
            municipio.Add(new Municipio { CodEstado = "13", CodMunicipio = "99", Nombre = "LA MIEL" });


            //14 MERIDA
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "01", Nombre = "EL VIGIA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "02", Nombre = "LA AZULITA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "03", Nombre = "SANTA CRUZ DE MORA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "04", Nombre = "ARICAGUA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "05", Nombre = "CANAGUA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "06", Nombre = "EJIDO" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "07", Nombre = "TUCANI" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "08", Nombre = "SANTO DOMINGO" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "09", Nombre = "GUARAQUE" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "10", Nombre = "ARAPUEY" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "11", Nombre = "TORONDOY" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "12", Nombre = "MERIDA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "13", Nombre = "TIMOTES" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "14", Nombre = "SANTA ELENA DE ARENALES" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "15", Nombre = "SANTA MARIA DE CAPARO" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "16", Nombre = "PUEBLO LLANO" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "17", Nombre = "MUCUCHIES" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "18", Nombre = "BAILADORES" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "19", Nombre = "TABAY" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "20", Nombre = "LAGUNILLAS" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "21", Nombre = "TOVAR" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "22", Nombre = "NUEVA BOLIVIA" });
            municipio.Add(new Municipio { CodEstado = "14", CodMunicipio = "23", Nombre = "ZEA" });

            //15 MIRANDA
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "01", Nombre = "CAUCAGUA" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "02", Nombre = "SAN JOSE DE BARLOVENTO" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "03", Nombre = "BARUTA" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "04", Nombre = "HIGUEROTE" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "05", Nombre = "MAMPORAL" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "06", Nombre = "CARRIZAL" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "07", Nombre = "CHACAO" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "08", Nombre = "CHARALLAVE" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "09", Nombre = "EL HATILLO" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "10", Nombre = "LOS TEQUES" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "11", Nombre = "SANTA TERESA DEL TUY" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "12", Nombre = "OCUMARE DEL TUY" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "13", Nombre = "SAN ANTONIO DE LOS ALTOS" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "14", Nombre = "RIO CHICO" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "15", Nombre = "SANTA LUCIA" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "16", Nombre = "CUPIRA" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "17", Nombre = "GUARENAS" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "18", Nombre = "SAN FRANCISCO DE YARE" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "19", Nombre = "PETARE" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "20", Nombre = "CUA" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "21", Nombre = "GUATIRE" });
            municipio.Add(new Municipio { CodEstado = "15", CodMunicipio = "22", Nombre = "CHUAO" });

            //16 MONAGAS
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "01", Nombre = "SAN ANTONIO" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "02", Nombre = "AGUASAY" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "03", Nombre = "CARIPITO" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "04", Nombre = "CARIPE" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "05", Nombre = "CAICARA" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "06", Nombre = "PUNTA DE MATA" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "07", Nombre = "TEMBLADOR" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "08", Nombre = "MATURIN" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "09", Nombre = "ARAGUA" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "10", Nombre = "QUIRIQUIRE" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "11", Nombre = "SANTA BARBARA" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "12", Nombre = "BARRANCAS" });
            municipio.Add(new Municipio { CodEstado = "16", CodMunicipio = "13", Nombre = "URACOA" });

            //25 NUEVA ESPARTA
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "01", Nombre = "IRIBARRE" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "02", Nombre = "PLAZA PARAGUACHI" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "03", Nombre = "LA ASUNCION" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "04", Nombre = "SAN JUAN BAUTISTA" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "05", Nombre = "VALLE DEL ESPIRITU STO" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "06", Nombre = "SANTA ANA" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "07", Nombre = "PAMPATAR" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "08", Nombre = "JUAN GRIEGO" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "09", Nombre = "PORLAMAR" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "10", Nombre = "BOCA DEL RIO" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "11", Nombre = "PUNTA DE PIEDRAS" });
            municipio.Add(new Municipio { CodEstado = "25", CodMunicipio = "12", Nombre = "SAN PEDRO DE COCHE" });

            //18 PORTUGUESA
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "01", Nombre = "SAN ANTONIO" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "02", Nombre = "AGUASAY" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "03", Nombre = "CARIPITO" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "04", Nombre = "CARIPE" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "05", Nombre = "CAICARA" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "06", Nombre = "PUNTA DE MATA" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "07", Nombre = "TEMBLADOR" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "08", Nombre = "MATURIN" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "09", Nombre = "ARAGUA" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "10", Nombre = "QUIRIQUIRE" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "11", Nombre = "SANTA BARBARA" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "12", Nombre = "BARRANCAS" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "13", Nombre = "URACOA" });
            municipio.Add(new Municipio { CodEstado = "18", CodMunicipio = "14", Nombre = "URACOA" });

            //19 SUCRE
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "01", Nombre = "CASANAY" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "02", Nombre = "SAN JOSE DE AEROCUAR" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "03", Nombre = "RIO CARIBE" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "04", Nombre = "EL PILAR" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "05", Nombre = "CARUPANO" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "06", Nombre = "MARIGUITAR" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "07", Nombre = "YAGUARAPARO" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "08", Nombre = "ARAYA" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "09", Nombre = "TUNAPUY" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "10", Nombre = "IRAPA" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "11", Nombre = "SAN ANTONIO DEL GOLFO" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "12", Nombre = "CUMANACOA" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "13", Nombre = "CARIACO" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "14", Nombre = "CUMANA" });
            municipio.Add(new Municipio { CodEstado = "19", CodMunicipio = "15", Nombre = "GUIRIA" });

            //20 TACHIRA
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "01", Nombre = "CORDERO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "02", Nombre = "LAS MESAS" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "03", Nombre = "COLON" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "04", Nombre = "SAN ANTONIO DEL TACHIRA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "05", Nombre = "TARIBA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "06", Nombre = "SANTA ANA DEL TACHIRA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "07", Nombre = "SAN RAFAEL DEL PIÑAL" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "08", Nombre = "SAN JOSE DE BOLIVAR" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "09", Nombre = "LA FRIA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "10", Nombre = "PALMIRA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "11", Nombre = "CAPACHO NUEVO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "12", Nombre = "LA GRITA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "13", Nombre = "EL COBRE" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "14", Nombre = "RUBIO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "15", Nombre = "CAPACHO VIEJO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "16", Nombre = "ABEJALES" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "17", Nombre = "LOBATERA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "18", Nombre = "MICHELENA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "19", Nombre = "COLONCITO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "20", Nombre = "UREÑA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "21", Nombre = "DELICIAS" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "22", Nombre = "LA TENDIDA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "23", Nombre = "SAN CRISTOBAL" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "24", Nombre = "SEBORUCO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "25", Nombre = "SAN SIMON" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "26", Nombre = "QUENIQUEA" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "27", Nombre = "SAN JOSECITO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "28", Nombre = "PREGONERO" });
            municipio.Add(new Municipio { CodEstado = "20", CodMunicipio = "29", Nombre = "UMUQUENA" });

            //21 TRUJILLO
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "01", Nombre = "SANTA ISABEL" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "02", Nombre = "BOCONO" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "03", Nombre = "SABANA GRANDE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "04", Nombre = "CHEJENDE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "05", Nombre = "CARACHE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "06", Nombre = "ESCUQUE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "07", Nombre = "EL PARADERO" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "08", Nombre = "CAMPO ELIAS" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "09", Nombre = "SANTA APOLONIA" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "10", Nombre = "EL DIVIDIVE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "11", Nombre = "MONTE CARMELO" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "12", Nombre = "MOTATAN" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "13", Nombre = "PAMPAN" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "14", Nombre = "PAMPANITO" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "15", Nombre = "BETIJOQUE" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "16", Nombre = "CARVAJAL" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "17", Nombre = "SABANA DE MENDOZA" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "18", Nombre = "TRUJILLO" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "19", Nombre = "LA QUEBRADA" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "20", Nombre = "VALERA" });
            municipio.Add(new Municipio { CodEstado = "21", CodMunicipio = "21", Nombre = "MONAY" });

            // 24 VARGAS
            municipio.Add(new Municipio { CodEstado = "24", CodMunicipio = "01", Nombre = "LA GUAIRA" });

            //22 YARACUY
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "01", Nombre = "SAN PABLO" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "02", Nombre = "AROA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "03", Nombre = "CHIVACOA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "04", Nombre = "COCOROTE" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "05", Nombre = "INDEPENDENCIA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "06", Nombre = "SABANA DE PARRA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "07", Nombre = "BORAURE" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "08", Nombre = "YUMARE" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "09", Nombre = "NIRGUA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "10", Nombre = "YARITAGUA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "11", Nombre = "SAN FELIPE" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "12", Nombre = "GUAMA" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "13", Nombre = "URACHICHE" });
            municipio.Add(new Municipio { CodEstado = "22", CodMunicipio = "14", Nombre = "FARRIAR" });

            //23 ZULIA
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "01", Nombre = "EL TORO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "02", Nombre = "SAN TIMOTEO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "03", Nombre = "CABIMAS" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "04", Nombre = "ENCONTRADOS" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "05", Nombre = "SAN CARLOS DEL ZULIA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "06", Nombre = "PUEBLO NUEVO(EL CHIVO)" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "07", Nombre = "LA CONCEPCION" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "08", Nombre = "CASIGUA EL CUBO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "09", Nombre = "CONCEPCION" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "10", Nombre = "CIUDAD OJEDA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "11", Nombre = "MACHIQUES" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "12", Nombre = "SAN RAFAEL DE EL MOJAN" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "13", Nombre = "MARACAIBO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "14", Nombre = "LOS PUERTOS DE ALTAGRACIA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "15", Nombre = "SINAMAICA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "16", Nombre = "LA VILLA DEL ROSARIO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "17", Nombre = "SAN FRANCISCO " });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "18", Nombre = "SANTA RITA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "19", Nombre = "TIA JUANA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "20", Nombre = "BOBURES" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "21", Nombre = "BACHAQUERO" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "22", Nombre = "SANTA BARBARA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "23", Nombre = "CAJA SECA" });
            municipio.Add(new Municipio { CodEstado = "23", CodMunicipio = "24", Nombre = "EL GUAYABO" });






            //PickerMunicipio.ItemsSource = municipio.Where(e => e.CodEstado == _codEstado).OrderBy(e => e.Nombre).ToList();
            //PickerMunicipio.Title = "Capital Municipio";


        }
        // GET: V_Productos/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            V_Productos v_Productos = await db.V_Productos.FindAsync(id);
            if (v_Productos == null)
            {
                return HttpNotFound();
            }
            return View(v_Productos);
        }

        // GET: V_Productos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: V_Productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PRODUCTO,DESCRIPCION,CANTIDAD_X_CAJA,UNIDAD_COSTEO")] V_Productos v_Productos)
        {
            if (ModelState.IsValid)
            {
                db.V_Productos.Add(v_Productos);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(v_Productos);
        }

        // GET: V_Productos/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            V_Productos v_Productos = await db.V_Productos.FindAsync(id);
            if (v_Productos == null)
            {
                return HttpNotFound();
            }
            return View(v_Productos);
        }

        // POST: V_Productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PRODUCTO,DESCRIPCION,CANTIDAD_X_CAJA,UNIDAD_COSTEO")] V_Productos v_Productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(v_Productos).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(v_Productos);
        }

        // GET: V_Productos/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            V_Productos v_Productos = await db.V_Productos.FindAsync(id);
            if (v_Productos == null)
            {
                return HttpNotFound();
            }
            return View(v_Productos);
        }

        // POST: V_Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            V_Productos v_Productos = await db.V_Productos.FindAsync(id);
            db.V_Productos.Remove(v_Productos);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }



        // GET: PreciosStockEstadoMunicipios/Edit/5
        public async Task<ActionResult> EditPrice(int? id)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return HttpNotFound();
            }
            return View(preciosStockEstadoMunicipio);
        }

        // POST: PreciosStockEstadoMunicipios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPrice([Bind(Include = "Id,Producto,Estado,Municipio,Descripcion,NombreEstado,NombreMunicipio,CapitalMunicipio,PrecioMinimo,PrecioMaximo,IdCalculo,FechaCalculo")] PreciosStockEstadoMunicipios preciosStockEstadoMunicipio)
        {
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            if (ModelState.IsValid)
            {
                db.Entry(preciosStockEstadoMunicipio).State = EntityState.Modified;
                await db.SaveChangesAsync();
                db.Database.ExecuteSqlCommand("[dbo].[RecalcularPrecioEstadoMunicipio] @p0, @p1,@p2", parameters: new[] { preciosStockEstadoMunicipio.Producto.Trim(), preciosStockEstadoMunicipio.Estado.Trim(), preciosStockEstadoMunicipio.Municipio.Trim() });

                return RedirectToAction("DetailPrice", new { id = preciosStockEstadoMunicipio.Producto.Trim() });
                                         
            }
            return View(preciosStockEstadoMunicipio);
        }


        // GET: ConsultaPrecios/Edit/5
        public async Task<ActionResult> ConsultaPrecios(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaPrecio consultaPrecio = new ConsultaPrecio();
            consultaPrecio.Producto = id;
            consultaPrecio.Estado = "";
            consultaPrecio.Municipio = "";
            consultaPrecio.PrecioMinimo = 0;
            consultaPrecio.PrecioMaximo = 0;
            ViewBag.Estado = new SelectList(db.VEstados.OrderBy(e => e.NOMBRE_ESTADO), "CODIGO_ESTADO", "NOMBRE_ESTADO");
            ViewBag.Municipio = new SelectList(db.VMunicipios.Where(m=>m.CODIGO_ESTADO== db.VEstados.OrderBy(e => e.NOMBRE_ESTADO).FirstOrDefault().CODIGO_ESTADO).OrderBy(m => m.CAPITAL_MCPO), "CODIGO_MCPO", "CAPITAL_MCPO");
            ViewBag.PrecioMinimo = consultaPrecio.PrecioMinimo;
            ViewBag.PrecioMaximo = consultaPrecio.PrecioMaximo;


            if (consultaPrecio == null)
            {
                return HttpNotFound();
            }
            return View(consultaPrecio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConsultaPrecios(ConsultaPrecio consultaPrecio)
        {

            PreciosStockEstadoMunicipios preciosStockEstadoMunicipios = new PreciosStockEstadoMunicipios();
            if (Session["IdRol"] == null)
            {
                return RedirectToAction("Login", "User");
            }
           
               
            db.Database.ExecuteSqlCommand("[dbo].[RecalcularPrecioEstadoMunicipio] @p0, @p1,@p2", parameters: new[] { consultaPrecio.Producto.Trim(), consultaPrecio.Estado.Trim(), consultaPrecio.Municipio.Trim() });
            preciosStockEstadoMunicipios =  db.PreciosStockEstadoMunicipios.Where(p => p.Producto == consultaPrecio.Producto && p.Estado == consultaPrecio.Estado && p.Municipio == consultaPrecio.Municipio).FirstOrDefault();
            if (preciosStockEstadoMunicipios!=null)
            {
                consultaPrecio.PrecioMinimo = preciosStockEstadoMunicipios.PrecioMinimo;
                consultaPrecio.PrecioMaximo = preciosStockEstadoMunicipios.PrecioMaximo;
               
            }

            var minimo = DoFormat((double)consultaPrecio.PrecioMinimo);


            ViewBag.PrecioMinimo = minimo;
            ViewBag.PrecioMaximo = consultaPrecio.PrecioMaximo;

            ViewBag.Estado = new SelectList(db.VEstados.OrderBy(e=>e.NOMBRE_ESTADO), "CODIGO_ESTADO", "NOMBRE_ESTADO", consultaPrecio.Estado);
            ViewBag.Municipio = new SelectList(db.VMunicipios.Where(m => m.CODIGO_ESTADO == consultaPrecio.Estado).OrderBy(m => m.CAPITAL_MCPO), "CODIGO_MCPO", "CAPITAL_MCPO", consultaPrecio.Municipio);
      

            //return RedirectToAction("DetailPrice", new { id = preciosStockEstadoMunicipio.Producto.Trim() });


            return View(consultaPrecio);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static string DoFormat(double myNumber)
        {

           

            var s =  String.Format(CultureInfo.InvariantCulture,
            "{0:0,0.00}", myNumber);
            return s;
          
        }
    }
}
