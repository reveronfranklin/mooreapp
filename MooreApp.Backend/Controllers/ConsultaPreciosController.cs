﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Backend.Controllers
{
    public class ConsultaPreciosController : Controller
    {
        private DataContext db = new DataContext();

        // GET: ConsultaPrecios
        public async Task<ActionResult> Index()
        {
            return View(await db.ConsultaPrecios.ToListAsync());
        }

        // GET: ConsultaPrecios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaPrecio consultaPrecio = await db.ConsultaPrecios.FindAsync(id);
            if (consultaPrecio == null)
            {
                return HttpNotFound();
            }
            return View(consultaPrecio);
        }

        // GET: ConsultaPrecios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ConsultaPrecios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Producto,Estado,Municipio,PrecioMinimo,PrecioMaximo")] ConsultaPrecio consultaPrecio)
        {
            if (ModelState.IsValid)
            {
                db.ConsultaPrecios.Add(consultaPrecio);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(consultaPrecio);
        }

        // GET: ConsultaPrecios/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaPrecio consultaPrecio = new ConsultaPrecio();
            consultaPrecio.Producto = id;
            consultaPrecio.Estado = "";
            consultaPrecio.Municipio = "";
            consultaPrecio.PrecioMinimo = 0;
            consultaPrecio.PrecioMaximo=0;


 
            if (consultaPrecio == null)
            {
                return HttpNotFound();
            }
            return View(consultaPrecio);
        }

        // POST: ConsultaPrecios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Producto,Estado,Municipio,PrecioMinimo,PrecioMaximo")] ConsultaPrecio consultaPrecio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consultaPrecio).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(consultaPrecio);
        }

        // GET: ConsultaPrecios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaPrecio consultaPrecio = await db.ConsultaPrecios.FindAsync(id);
            if (consultaPrecio == null)
            {
                return HttpNotFound();
            }
            return View(consultaPrecio);
        }

        // POST: ConsultaPrecios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ConsultaPrecio consultaPrecio = await db.ConsultaPrecios.FindAsync(id);
            db.ConsultaPrecios.Remove(consultaPrecio);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
