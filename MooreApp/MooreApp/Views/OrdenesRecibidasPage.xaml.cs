﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MooreApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrdenesRecibidasPage : ContentPage
	{
		public OrdenesRecibidasPage ()
		{
			InitializeComponent ();
            //Browser.Source = "https://mooreapps.com.ve/MooreAppBackEnd/VisualizeData/TablaOrdenes";
          
            Device.OpenUri(new Uri("https://mooreapps.com.ve/MooreAppBackEnd/VisualizeData/TablaOrdenes"));
        }
	}
}