﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Backend.Controllers
{
    public class WSMY501Controller : Controller
    {
        private DataContext db = new DataContext();

        // GET: WSMY501
        public async Task<ActionResult> Index()
        {
            return View(await db.WSMY501.Where(w => w.Fecha.Value.Year == 2020).ToListAsync());
        }

        // GET: WSMY501/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WSMY501 wSMY501 = await db.WSMY501.FindAsync(id);
            if (wSMY501 == null)
            {
                return HttpNotFound();
            }
            return View(wSMY501);
        }

        // GET: WSMY501/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WSMY501/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Cotizacion,Cod_Vendedor,Cod_Cliente,Prospecto,Fecha,Observaciones,Fecha_Caducidad,Fecha_Postergada,Estatus,Razon_Social,Tipo,Sector,Ramo,Email_Cliente,Rif,Condicion,Id_Contacto,Observacion_Postergar,Fecha_EnEspera,Fecha_Cerrada,UsuarioActualiza,FechaActualiza,NombreContacto,TelefonoContacto,EmailContacto,Id_direccion,DireccionFacturar,DireccionEntregar,FlagWebSite,RifEntregar,OrdenCompra,IdMedio,FlagActualizado,Id_direccionEntregar,EstadoFacturar,MunicipioFacturar,EstadoEntregar,MunicipioEntregar,FlagValidado,FechaReactivacion,IdClienteProspecto,idsolicitudCredito,UltimoEvaluadorCredito,EvaluadaAdministradora,CambioPrecio,Noreplicar,IdTipoTransaccion,NegocioEnUsd,FijarPrecioBs,FijarPrecioBsAprobado,FechaPago,ObservacionPago")] WSMY501 wSMY501)
        {
            if (ModelState.IsValid)
            {
                db.WSMY501.Add(wSMY501);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(wSMY501);
        }

        // GET: WSMY501/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WSMY501 wSMY501 = await db.WSMY501.FindAsync(id);
            if (wSMY501 == null)
            {
                return HttpNotFound();
            }
            return View(wSMY501);
        }

        // POST: WSMY501/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Cotizacion,Cod_Vendedor,Cod_Cliente,Prospecto,Fecha,Observaciones,Fecha_Caducidad,Fecha_Postergada,Estatus,Razon_Social,Tipo,Sector,Ramo,Email_Cliente,Rif,Condicion,Id_Contacto,Observacion_Postergar,Fecha_EnEspera,Fecha_Cerrada,UsuarioActualiza,FechaActualiza,NombreContacto,TelefonoContacto,EmailContacto,Id_direccion,DireccionFacturar,DireccionEntregar,FlagWebSite,RifEntregar,OrdenCompra,IdMedio,FlagActualizado,Id_direccionEntregar,EstadoFacturar,MunicipioFacturar,EstadoEntregar,MunicipioEntregar,FlagValidado,FechaReactivacion,IdClienteProspecto,idsolicitudCredito,UltimoEvaluadorCredito,EvaluadaAdministradora,CambioPrecio,Noreplicar,IdTipoTransaccion,NegocioEnUsd,FijarPrecioBs,FijarPrecioBsAprobado,FechaPago,ObservacionPago")] WSMY501 wSMY501)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wSMY501).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(wSMY501);
        }

        // GET: WSMY501/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WSMY501 wSMY501 = await db.WSMY501.FindAsync(id);
            if (wSMY501 == null)
            {
                return HttpNotFound();
            }
            return View(wSMY501);
        }

        // POST: WSMY501/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            WSMY501 wSMY501 = await db.WSMY501.FindAsync(id);
            db.WSMY501.Remove(wSMY501);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
