﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class AppBacklogInicial
    {
        [Key]
        public int Id { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }
        public int CantBacklogInicial { get; set; }
        public decimal BsBacklogInicial { get; set; }
        public decimal USDBacklogInicial { get; set; }



    }
}
