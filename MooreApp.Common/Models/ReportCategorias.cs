﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class ReportCategorias
    {
        public int IdCategoria { get; set; }

        public string  NombreCategoria { get; set; }

        public int IdSubCategoria { get; set; }

        public string NombreSubCategoria { get; set; }

        public decimal ?Embarque_Bruto { get; set; }
        public decimal ?Transito { get; set; }
        public decimal ?Notas_de_Debito { get; set; }
        public decimal ?NE_de_FA_Adelantadas { get; set; }
        public decimal ?Adelantado_X_Entregar { get; set; }
        public decimal ?NC_FA_Adelantadas { get; set; }


        public decimal ?TotalVenta { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }

    }
}
