﻿using MooreApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MooreApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
	{
        //   xmlns:controls="clr-namespace:ImageCircle.Forms.Plugin.Abstractions;assembly=ImageCircle.Forms.Plugin"
        public MenuPage ()
		{
			InitializeComponent ();
		}
        public  void MenuClicked(object sender, ItemTappedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)e.Item;

            var main = MainViewModel.GetInstance().MenuItem;
            
            main.GotoCommand.Execute(this);

     
        }
    }
}