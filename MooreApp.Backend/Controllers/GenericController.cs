﻿using MooreApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MooreApp.Backend.Controllers
{
    public class GenericController : Controller
    {


        private DataContext db = new DataContext();


        public JsonResult GetMunicipios(string estado)
        {
         
            db.Configuration.ProxyCreationEnabled = false;
            var municipios = db.VMunicipios.Where(m => m.CODIGO_ESTADO == estado).OrderBy(o => o.CAPITAL_MCPO);
            return Json(municipios);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}