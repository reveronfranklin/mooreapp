﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class CotizacionResponse
    {

        [StringLength(13)]
        public string Cotizacion { get; set; }

        [StringLength(4)]
        public string Cod_Vendedor { get; set; }
        
        public string NombreVendedor { get; set; }

        [StringLength(10)]
        public string Cod_Cliente { get; set; }

        [StringLength(80)]
        public string Razon_Social { get; set; }

        public bool? FijarPrecioBs { get; set; }

        public bool? FijarPrecioBsAprobado { get; set; }

        public DateTime? FechaPago { get; set; }

        public string ObservacionPago { get; set; }

        public List<DetalleCotizacionResponse> DetalleCotizacionResponse { get; set; }

        public string FijarPrecioBsTexto { get; set; }

        public string FijarPrecioBsAprobadoTexto { get; set; }



    }
}
