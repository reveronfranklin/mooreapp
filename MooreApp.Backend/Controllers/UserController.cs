﻿using MooreApp.Common.Models;
using MooreApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;



namespace MooreApp.Backend.Controllers
{
    [Authorize]
    public class UserController : Controller
    {

        private ClsUtilitario Util = new ClsUtilitario();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
      
        public ActionResult Login(string returnUrl, string sha1 = null)
        {

            sha1 = Request.QueryString["CODSEG"];
           
           

            ViewBag.ReturnUrl = returnUrl;

            if (sha1 != null)
            {

                string Usuario = BuscarUsuarioToken(sha1);

                if (Usuario != "")
                {
                    if (UsuarioValidoToken(Usuario))
                    {
                        FormsAuthentication.SetAuthCookie(Usuario.ToUpper(), false);
                        return RedirectToAction("Index", "Home");
                        

                    }
                }
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(ModeloUsuario user, string returnUrl)
        {

            string sha1 = Request.QueryString["CODSEG"];
        
            ViewBag.ReturnUrl = returnUrl;

            if (sha1 != null)
            {

                string Usuario = BuscarUsuarioToken(sha1);

                if (Usuario != "")
                {
                    if (UsuarioValidoToken(Usuario))
                    {
                        FormsAuthentication.SetAuthCookie(Usuario.ToUpper(), false);
                        return RedirectToAction("Index", "Home");


                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (UsuarioValido(user.Usuario,user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.Usuario.ToUpper(), false);

                    if (returnUrl != null)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }                    

                }
                else
                {
                    ModelState.AddModelError("", "Información de logeo incorrecta");
                }
            }

            return View(user);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registrar()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CerrarSesion()
        {
            FormsAuthentication.SignOut();

            Session.Abandon();

            return RedirectToAction("Login", "User");
        }

        [HttpPost]
        public ActionResult Registrar(ModeloUsuario user)
        {
            return View();
        }

        private bool UsuarioValido(string Usuario, string Password, string ReturnUrl = null){

            bool Validado = false;

            using (var db = new DataContext())
            {

                var BuscarUsuario = (from u in db.SegUsuario
                                    join ur in db.SegUsuarioRol on u.IdUsuario equals ur.IdUsuario
                                    join r in db.SegRol on ur.IdRol equals r.IdRol
                                    where u.Usuario == Usuario & r.IdPrograma == 148
                                    select new { Clave = u.Clave, IdRol = r.IdRol, NombreUsuario = u.NombreUsuario }).FirstOrDefault();

                if (BuscarUsuario != null)
                {

                    if (ReturnUrl == null)
                    {

                        if (BuscarUsuario.Clave != ClsUtilitario.ConvertSha1(Password))
                        {

                            Session["NombreUsuario"] = BuscarUsuario.NombreUsuario;

                            Session["IdRol"] = BuscarUsuario.IdRol;

                            Validado = true;

                        }
                    }
                    else
                    {

                        Session["NombreUsuario"] = BuscarUsuario.NombreUsuario;

                        Session["IdRol"] = BuscarUsuario.IdRol;

                        Validado = true;

                    }

                    

                }

            }

            return Validado;

        }


        private bool UsuarioValidoToken(string Usuario)
        {

            bool Validado = false;

            using (var db = new DataContext())
            {

                var BuscarUsuario = (from u in db.SegUsuario
                                     join ur in db.SegUsuarioRol on u.IdUsuario equals ur.IdUsuario
                                     join r in db.SegRol on ur.IdRol equals r.IdRol
                                     where u.Usuario == Usuario & r.IdPrograma == 148
                                     select new { Clave = u.Clave, IdRol = r.IdRol, NombreUsuario = u.NombreUsuario }).FirstOrDefault();

                if (BuscarUsuario != null)
                {
                    Session["NombreUsuario"] = BuscarUsuario.NombreUsuario;

                    Session["IdRol"] = BuscarUsuario.IdRol;

                    Validado = true;
                   



                }

            }

            return Validado;

        }


        private string BuscarUsuarioSha1(string sha1)
        {

            using (var db = new DataContext())
            {

                SegTicket Seg = db.SegTicket.Find(sha1);

                if (Seg.FlagUsado == false)
                {
                    db.Entry(Seg).State = EntityState.Modified;

                    Seg.FlagUsado = true;

                    Seg.FechaHoraUso = DateTime.Now;

                    db.SaveChanges();

                    return Seg.IdUsuario.ToUpper();
                }
                else
                {
                    return "";
                }
            }

        }

        private string BuscarUsuarioToken(string sha1)
        {

            using (var db = new DataContext())
            {

                WITY020 Seg = db.WITY020.Find(sha1);

                if (Seg.UTILIZADO == "")
                {
                    db.Entry(Seg).State = EntityState.Modified;

                    return Seg.USUARIO.ToUpper();
                }
                else
                {
                    return "";
                }
            }

        }
    }
}
