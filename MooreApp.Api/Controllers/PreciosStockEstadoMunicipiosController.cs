﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MooreApp.Api.Models;
using MooreApp.Common.Models;
using MooreApp.Domain.Models;

namespace MooreApp.Api.Controllers
{
    public class PreciosStockEstadoMunicipiosController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/PreciosStockEstadoMunicipios
        public IQueryable<PreciosStockEstadoMunicipios> GetPreciosStockEstadoMunicipios()
        {
            return db.PreciosStockEstadoMunicipios;
        }

        // GET: api/PreciosStockEstadoMunicipios/5
        [ResponseType(typeof(PreciosStockEstadoMunicipios))]
        public async Task<IHttpActionResult> GetPreciosStockEstadoMunicipio(int id)
        {
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return NotFound();
            }

            return Ok(preciosStockEstadoMunicipio);
        }

        // P    UT: api/PreciosStockEstadoMunicipios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPreciosStockEstadoMunicipio(int id, PreciosStockEstadoMunicipios preciosStockEstadoMunicipio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != preciosStockEstadoMunicipio.Id)
            {
                return BadRequest();
            }

            db.Entry(preciosStockEstadoMunicipio).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PreciosStockEstadoMunicipioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PreciosStockEstadoMunicipios
        //[ResponseType(typeof(PreciosStockEstadoMunicipio))]
        //public async Task<IHttpActionResult> PostPreciosStockEstadoMunicipio(PreciosStockEstadoMunicipio preciosStockEstadoMunicipio)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.PreciosStockEstadoMunicipios.Add(preciosStockEstadoMunicipio);
        //    await db.SaveChangesAsync();

        //    return CreatedAtRoute("DefaultApi", new { id = preciosStockEstadoMunicipio.Id }, preciosStockEstadoMunicipio);
        //}


        [ResponseType(typeof(PreciosStockEstadoMunicipios))]
        public List<PreciosStockEstadoMunicipios> PostPreciosStockEstadoMunicipio([FromBody]  ProductRequest productViewModel)
        {
            List<PreciosStockEstadoMunicipios> result = new List<PreciosStockEstadoMunicipios>();

            try
            {
                //Internamente Ejecuta  exec mc.[dbo].[PaActualizaListaDePrecioStockFlete] @Producto,@Estado,@Municipio

                db.Database.ExecuteSqlCommand("[dbo].[RecalcularPrecioEstadoMunicipio] @p0, @p1,@p2", parameters: new[] { productViewModel.Producto, productViewModel.Estado, productViewModel.Municipio });

                result = db.PreciosStockEstadoMunicipios.Where(p => p.Producto == productViewModel.Producto && p.Estado == productViewModel.Estado && p.Municipio == productViewModel.Municipio).ToList();

                return result;
               
                

            }
            catch (Exception)
            {
                return null;
               


            }



        }

        // DELETE: api/PreciosStockEstadoMunicipios/5
        [ResponseType(typeof(PreciosStockEstadoMunicipios))]
        public async Task<IHttpActionResult> DeletePreciosStockEstadoMunicipio(int id)
        {
            PreciosStockEstadoMunicipios preciosStockEstadoMunicipio = await db.PreciosStockEstadoMunicipios.FindAsync(id);
            if (preciosStockEstadoMunicipio == null)
            {
                return NotFound();
            }

            db.PreciosStockEstadoMunicipios.Remove(preciosStockEstadoMunicipio);
            await db.SaveChangesAsync();

            return Ok(preciosStockEstadoMunicipio);
        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, System.Web.Http.Route("api/PreciosStockEstadoMunicipios/GetProducto/")]
        public List<V_Productos> GetProducto([FromBody]  ProductRequest productViewModel)
        {
            if (!GeUser(productViewModel.Usuario, productViewModel.Password))
            {
                return null;
            }

            List<V_Productos> result = new List<V_Productos>();

            result = db.V_Productos.OrderBy(P => P.PRODUCTO).ToList();
                        
            return result;
        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, System.Web.Http.Route("api/PreciosStockEstadoMunicipios/GeUser/")]
        public List<WITY021> GeUser([FromBody]  LoginRequest loginRequest)
        {
            List<WITY021> result = new List<WITY021>();
           ClsUtilitario Util = new ClsUtilitario();
           
            string pass = ClsUtilitario.ConvertSha1(loginRequest.Password);
          
            var user = db.WITY021.Where(u => u.Usuario == loginRequest.Usuario && u.Clave == pass && u.Inactivo == "").FirstOrDefault();
            if (user!=null)
            {
                result.Add(user);
            }
         


            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PreciosStockEstadoMunicipioExists(int id)
        {
            return db.PreciosStockEstadoMunicipios.Count(e => e.Id == id) > 0;
        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, System.Web.Http.Route("api/PreciosStockEstadoMunicipios/EmbarquesMes/")]
        public List<ReportCategorias> EmbarquesMes([FromBody]  AñoMesRequest añoMesRequest)
        {

            if (!GeUser(añoMesRequest.Usuario, añoMesRequest.Password))
            {
                return null;
            }

            List<ReportCategorias> resultOri = new List<ReportCategorias>();
            List<ReportCategorias> result = new List<ReportCategorias>();

            try
            {

                 resultOri = db.Database.SqlQuery<ReportCategorias>("exec [dbo].[Sp_App_ConsultaEmbarquesAñoMes] @p0, @p1", añoMesRequest.Año, añoMesRequest.Mes).ToList<ReportCategorias>();

                foreach (var item in resultOri)
                {
                    if (item.Embarque_Bruto == null)
                    {
                        item.Embarque_Bruto = 0;
                    }
                    if (item.Transito == null)
                    {
                        item.Transito = 0;
                    }
                    if (item.Notas_de_Debito == null)
                    {
                        item.Notas_de_Debito = 0;
                    }
                    if (item.NE_de_FA_Adelantadas == null)
                    {
                        item.NE_de_FA_Adelantadas = 0;
                    }
                    if (item.Adelantado_X_Entregar == null)
                    {
                        item.Adelantado_X_Entregar = 0;
                    }
                    if (item.NC_FA_Adelantadas == null)
                    {
                        item.NC_FA_Adelantadas = 0;
                    }

                    item.TotalVenta = 
                        (item.Embarque_Bruto )+
                        (item.Transito) + 
                        (item.Notas_de_Debito) +
                        (item.NE_de_FA_Adelantadas) +
                        (item.Adelantado_X_Entregar) +
                        (item.NC_FA_Adelantadas); 
                    result.Add(item);
                }
            
           
                return result;



            }
            catch (Exception e)
            {
                var a =e.Message;
                return null;



            }



        }
        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, System.Web.Http.Route("api/PreciosStockEstadoMunicipios/EmbarquesHistorico/")]
        public List<ReportCategoriaHistorico> EmbarquesHistorico([FromBody]  AñoMesRequest añoMesRequest)
        {

            if (!GeUser(añoMesRequest.Usuario, añoMesRequest.Password))
            {
                return null;
            }

            List<ReportCategoriaHistorico> resultOri = new List<ReportCategoriaHistorico>();
            List<ReportCategoriaHistorico> result = new List<ReportCategoriaHistorico>();

            try
            {

                resultOri = db.Database.SqlQuery<ReportCategoriaHistorico>("exec [dbo].[AppEmbarquesCategoriaHistorico] @p0, @p1", añoMesRequest.Año, añoMesRequest.Mes).ToList<ReportCategoriaHistorico>();

                foreach (var item in resultOri)
                {
                  

                    result.Add(item);
                }


                return result;



            }
            catch (Exception e)
            {
                var a = e.Message;
                return null;



            }



        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, System.Web.Http.Route("api/PreciosStockEstadoMunicipios/GetUsuarioPage/")]
        public List<AppUsuarioPages> GetUsuarioPage([FromBody]  LoginRequest loginRequest)
        {
            if (!GeUser(loginRequest.Usuario, loginRequest.Password))
            {
                return null;
            }

            List<AppUsuarioPages> result = new List<AppUsuarioPages>();

            result = db.AppUsuarioPages.Where(u => u.Usuario == loginRequest.Usuario ).ToList();



            return result;
        }

        public bool GeUser(string usuario,string password)
        {
            bool result = false;
           

            string pass = ClsUtilitario.ConvertSha1(password);

            var user = db.WITY021.Where(u => u.Usuario == usuario && u.Clave == pass && u.Inactivo == "").FirstOrDefault();
            if (user != null)
            {
                result=true;
            }



            return result;
        }


    }
}