﻿


namespace MooreApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using MooreApp.Common.Models;
    using MooreApp.Helpers;
    using MooreApp.Services;
    using MooreApp.Views;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class ProductsViewModel:BaseViewModel
    {
        private bool isRefreshing;
        private string filter;    

        private ApiServices apiServices;


      
        public List<V_Productos> list;
        private ObservableCollection<V_Productos> products;

        public ObservableCollection<V_Productos> Products
        {
            get{ return this.products; }
            set { this.SetValue(ref this.products, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }
        public string Filter
        {
            get { return this.filter; }
            set {
                this.filter=value;
                this.RefreshList();
            }
        }

        public ProductsViewModel()
        {
            this.apiServices = new ApiServices();
         
            LoadProducts();
           
        }



        private async void LoadProducts()
        {
            this.IsRefreshing = true;

         
            string urlBase = "https://mooreapps.com.ve/MooreApp";
            //var urlBase = Application.Current.Resources["UrlAPI"].ToString();

            var productsViewModel = new ProductRequest
            {
                Producto= "10HELP-001",
                Estado="01",
                Municipio="01",
                Usuario=Settings.Usuario,
                Password=Settings.Password,
                
  

            };

            var connection = await this.apiServices.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Sin Conexion a internet!", connection.Message, "Aceptar");
                return;
            }
            var response = await this.apiServices.GetListV_Producto<V_Productos>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", productsViewModel);

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Aceptar");

                return;
            }
            
            list = (List<V_Productos>)response.Result;
            this.Products = new ObservableCollection<V_Productos>(list);
            this.IsRefreshing = false;  
        }



        private async void LoadPrecioProducts()
        {
            this.IsRefreshing = true;
            string urlBase = "https://mooreapps.com.ve/MooreApp";


            var productsViewModel = new ProductRequest
            {
                Producto = "10HELP-001",
                Estado = "01",
                Municipio = "01",
                Usuario = Settings.Usuario,
                Password = Settings.Password,

            };


            var response = await this.apiServices.GetList<PreciosStockEstadoMunicipios>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", productsViewModel);

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Aceptar");

                return;
            }

            var list = (List<V_Productos>)response.Result;
            this.Products = new ObservableCollection<V_Productos>(list);
            this.IsRefreshing = false;
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadProducts);
            }
            
        }



        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(RefreshList);
            }

        }

        private void RefreshList()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                var myListProductAll = this.list.Select(p => new V_Productos
                {
                    DESCRIPCION = p.DESCRIPCION,
                    CANTIDAD_X_CAJA = p.CANTIDAD_X_CAJA,
                    PRODUCTO = p.PRODUCTO,
                    UNIDAD_COSTEO = p.UNIDAD_COSTEO,
                });
                this.Products = new ObservableCollection<V_Productos>(myListProductAll.OrderBy(p=> p.DESCRIPCION));


            }
            else
            {
                var myListProduct = this.list.Select(p => new V_Productos
                {
                    DESCRIPCION = p.DESCRIPCION,
                    CANTIDAD_X_CAJA = p.CANTIDAD_X_CAJA,
                    PRODUCTO = p.PRODUCTO,
                    UNIDAD_COSTEO = p.UNIDAD_COSTEO,
                }).Where(p => p.DESCRIPCION.ToLower().Contains(this.Filter.ToLower())).OrderBy(p => p.DESCRIPCION).ToList();
                this.Products = new ObservableCollection<V_Productos>(myListProduct);

            }

        }
    }
}
