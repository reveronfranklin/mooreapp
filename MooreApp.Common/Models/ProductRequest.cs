﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class ProductRequest
    {

        public string Producto { get; set; }

        public string Estado { get; set; }

        public string Municipio { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }
    }
}
