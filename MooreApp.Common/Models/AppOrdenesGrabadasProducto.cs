﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MooreApp.Common.Models
{
    public class AppOrdenesGrabadasProducto
    {

        [Key]
        public int Id { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }
        public DateTime Dia { get; set; }
        [Display(Name = "Fecha")]
        public string DiaChar { get; set; }

        public int CantFormas { get; set; }
        public decimal BsFormas { get; set; }
        public int CantEtiquetas { get; set; }
        public decimal BsEtiquetas { get; set; }
        public int CantICPP { get; set; }
        public decimal BsICPP { get; set; }
        public int CantCustomPrint { get; set; }
        public decimal BsCustomPrint { get; set; }
        public int CantOfficeProduct { get; set; }
        public decimal BsOfficeProduct { get; set; }
        public int CantTotal { get; set; }
        public decimal BsTotal { get; set; }
        public decimal VentaDolar { get; set; }


        public string BsFormasChar { get; set; }
   
        public string BsEtiquetasChar { get; set; }
  
        public string BsICPPChar { get; set; }
    
        public string BsCustomPrintChar { get; set; }
   
        public string BsOfficeProductChar { get; set; }
     
        public string BsTotalChar { get; set; }

        public string VentaDolarChar { get; set; }


    }
}
