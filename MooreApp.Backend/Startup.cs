﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MooreApp.Backend.Startup))]
namespace MooreApp.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
