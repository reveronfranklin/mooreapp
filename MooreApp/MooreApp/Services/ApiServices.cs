﻿namespace MooreApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Models;
    using Newtonsoft.Json;
    using Plugin.Connectivity;

    public class ApiServices
    {

        public async Task<Response> CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Por favor encienda su conexion a internet!!",
                };
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            if (!isReachable)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Sin Conexion a Internet!!",
                };
            }

            return new Response
            {
                IsSuccess = true,
            };
        }

        public async Task<Response> GetList<PreciosStockEstadoMunicipio>(string urlBase,string prefix,string controller,ProductRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "https://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios";

                var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url,body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<PreciosStockEstadoMunicipio>>(json);
                if (list==null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Datos no encontrados",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result=list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess=false,
                    Message=ex.Message,
                };
            }
        }

        public async Task<Response> GetListV_Producto<V_Producto>(string urlBase, string prefix, string controller, ProductRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "https://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios/GetProducto";

               var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url, body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<V_Producto>>(json);
                if (list == null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Datos no encontrados",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result = list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> GetListUser<WITY021>(string urlBase, string prefix, string controller, LoginRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "http://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios/GeUser";

                var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url, body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<WITY021>>(json);
                if (list == null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Usuario o clave invalidad",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result = list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> GetListReport<ReportCategorias>(string urlBase, string prefix, string controller, AñoMesRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "https://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios/EmbarquesMes";

                var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url, body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<ReportCategorias>>(json);
                if (list == null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Datos no encontrados",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result = list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> GetListReportHistorico<ReportCategorias>(string urlBase, string prefix, string controller, AñoMesRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "https://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios/EmbarquesHistorico";

                var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url, body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<ReportCategorias>>(json);
                if (list == null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Datos no encontrados",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result = list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<Response> GetListUserPage<AppUsuarioPage>(string urlBase, string prefix, string controller, LoginRequest model)
        {

            try
            {
                HttpClient cliente = new HttpClient();
                string url = "https://mooreapps.com.ve/MooreApp/Api/PreciosStockEstadoMunicipios/GetUsuarioPage";

                var request = JsonConvert.SerializeObject(model);
                var body = new StringContent(request, Encoding.UTF8, "application/json");

                var resultado = await cliente.PostAsync(url, body);
                var json = resultado.Content.ReadAsStringAsync().Result;
                var list = JsonConvert.DeserializeObject<List<AppUsuarioPage>>(json);
                if (list == null)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "Datos no encontrados",
                        Result = null,
                    };
                }
                return new Response
                {
                    IsSuccess = true,
                    Message = "",
                    Result = list,
                };
            }
            catch (Exception ex)
            {

                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }



    }
}
