﻿using GalaSoft.MvvmLight.Command;
using MooreApp.Common.Models;
using MooreApp.Helpers;
using MooreApp.Services;
using MooreApp.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Xamarin.Forms;
namespace MooreApp.ViewModels
{
    public class ReportCategoriasViewModel : BaseViewModel
    {
        private bool isRefreshing;
        private bool isEnable;
        private int año;
        private int mes;
       private decimal totalCustomPrint;
       private decimal totalOfficeProduct;
        private decimal total;
        private string periodo;
       
        private int height;
        private int height2;

        public List<ReportCategorias> list;
        private ObservableCollection<ReportCategorias> report;
        private ObservableCollection<ReportCategorias> report2;
        private ApiServices apiServices;

        public ObservableCollection<ReportCategorias> Report
        {
            get { return this.report; }
            set { this.SetValue(ref this.report, value); }
        }
        public ObservableCollection<ReportCategorias> Report2
        {
            get { return this.report2; }
            set { this.SetValue(ref this.report2, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }
        public bool IsEnable
        {
            get { return this.isEnable; }
            set { this.SetValue(ref this.isEnable, value); }
        }
        public int Año
        {
            get { return this.año; }
            set { this.SetValue(ref this.año, value); }
        }
        public int Mes
        {
            get { return this.mes; }
            set { this.SetValue(ref this.mes, value); }
        }
        public string Periodo
        {
            get { return this.periodo; }
            set { this.SetValue(ref this.periodo, value); }
        }

        public decimal TotalCustomPrint
        {
            get { return this.totalCustomPrint; }
            set { this.SetValue(ref this.totalCustomPrint, value); }
        }

        public decimal TotalOfficeProduct
        {
            get { return this.totalOfficeProduct; }
            set { this.SetValue(ref this.totalOfficeProduct, value); }
        }

        public decimal Total
        {
            get { return this.total; }
            set { this.SetValue(ref this.total, value); }
        }
        public int Height
        {
            get { return this.height; }
            set { this.SetValue(ref this.height, value); }
        }
        public int Height2
        {
            get { return this.height2; }
            set { this.SetValue(ref this.height2, value); }
        }

        public ReportCategoriasViewModel()
        {
            this.IsEnable = true;
            this.apiServices = new ApiServices();
            if (Año==0)
            {
      
                Año = DateTime.Now.Year;
            }
            if (Mes == 0)
            {
                Mes = DateTime.Now.Month;
               
            }

            
            LoadReport();
        }


        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadReport);
            }

        }

        private async void LoadReport()
        {
            this.IsRefreshing = true;
            this.IsEnable = false;

            if (Año <= 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe Indicar Año", "Aceptar");
                this.IsRefreshing = false;
                this.IsEnable = true;
                return;
            }
            if (Mes <= 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe Indicar Mes", "Aceptar");
                this.IsRefreshing = false;
                this.IsEnable = true;
                return;
            }


            string urlBase = "https://mooreapps.com.ve/MooreApp";
            //var urlBase = Application.Current.Resources["UrlAPI"].ToString();

            var añoMesRequest = new AñoMesRequest
            {
                Año=Año,
                Mes=Mes,
                Usuario = Settings.Usuario,
                Password = Settings.Password,

            };


            var response = await this.apiServices.GetListReport<ReportCategorias>(urlBase, "/Api", "/PreciosStockEstadoMunicipios", añoMesRequest);

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                this.IsEnable = true;
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Aceptar");

                return;
            }

            
            list = (List<ReportCategorias>)response.Result;
            this.Report = new ObservableCollection<ReportCategorias>(list.Where(r => r.IdCategoria == 1).ToList());
            this.Report2 = new ObservableCollection<ReportCategorias>(list.Where(r=> r.IdCategoria==2).ToList());
            var FirstRecord = list.Where(r => r.IdCategoria == 2).FirstOrDefault();
            if (FirstRecord !=null)
            {
                this.Periodo = "Mes: " + FirstRecord.Mes + " Año: " + FirstRecord.Año;
            }


            this.TotalCustomPrint = TotalReport(Report);
            this.TotalOfficeProduct = TotalReport(Report2);
            this.Total = this.TotalCustomPrint + this.TotalOfficeProduct;
            Height = (this.Report.Count * 30) ;
            Height2 = (this.Report2.Count * 30) + (this.Report2.Count * 10);
            this.IsRefreshing = false;
            this.IsEnable = true;
        }

        private decimal TotalReport(ObservableCollection<ReportCategorias> list)
        {
            decimal result = 0;
            foreach (var item in list)
            {
                result = result + (decimal)item.TotalVenta;
            }

            return result;
        }
        private async void BuscarButton_Clicked(object sender, EventArgs e)
        {

            if (Año <= 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe Indicar Año", "Aceptar");
                return;
            }
            if (Mes <= 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe Indicar Mes", "Aceptar");
                return;
            }
            var main = MainViewModel.GetInstance().ReportCategorias;

            main.RefreshCommand.Execute(this);



        }



    }
}
